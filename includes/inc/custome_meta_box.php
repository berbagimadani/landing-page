<?php

//making the meta box (Note: meta box != custom meta field)
function wpse_add_custom_meta_box_2() { 

   add_meta_box(
       'custom_meta_box-2',       // $id
       'Meta Box Movies',                  // $title
       'show_custom_meta_box_2',  // $callback
       'post',                  // $page
       'normal',                  // $context
       'high'                     // $priority
   );
}
add_action('add_meta_boxes', 'wpse_add_custom_meta_box_2');

function dataField() {
    
    $options = get_option('wpviddycpaattributes'); 
    $roman = new Walk();
    $out = $roman->optionWp2($options['attributes'],$options['attributes2']); 
    $roman->handleOptionWp($out);

	$data['id_imdb']				= '';
	$data['play']					= '';

	/* play */
	$data['player']					= 'youtube';
	$data['url_video']				= 'NZSxu-UKLV8';
	$data['url_image']				= 'http://img.123movies.to/2016/06/17/cover/32fa9e38de65f1ec2c29b69000b86325-orange-is-the-new-black-season-4-1466189608.jpg';

	$data['type_cta']				= 'openlink'; 
	$data['url_cta']				= 'http://yournetwork';

	//$data['locker']					= 1;
	//$data['url_referral']			= 'http://html.iwthemes.com/tv/';
	$data['duration_show_offer']	= 11;
 

    $data['line']                   = '';
	$data['content']						= '';

	/* optional */
	//$data['title'] 			= 'Orange is the New Black - Season 4';
	//$data['description']	= 'Who will fight, and who will fall? Season 4 opens with a major security breach and a lot of new inmates, therefore Caputo has to call in the big guns. Things get a little too real for Crazy Eyes and Lolly.';
	$data['image']			= 'http://img.123movies.to/2016/06/17/poster/062da9031c92a403efbcde9e1a2772bc-orange-is-the-new-black-season-4-1466189607.jpg';
	$data['rating']			= '9.0';
	$data['actors']			= 'Taylor Schilling, Danielle Brooks, Taryn Manning';
    $data['image_actors']   = 'x';
    $data['image_gallery']   = 'x';
	$data['genres']			= 'Comedy, Crime, Drama';
	$data['duration']		= '59 min';
	$data['release']		= '2016'; 
	$data['view_watch']		= '1000 view';

	return $data;
}

function show_custom_meta_box_2() {
    global $post;

    require_once plugin_dir_path( dirname( __FILE__ ) ) . 'inc/class-wpviddycpa-field.php'; 
	$gt = new Wpviddycpa_Field(); 

	$plugin_meta = 'wpviddycpa-meta';
    // Use nonce for verification to secure data sending
    wp_nonce_field( basename( __FILE__ ), 'wpse_our_nonce' );
   

    ?>
 	
 	<div class="uk-form-stacked-custome uk-form-horizontal">
 	<?php foreach ( dataField() as $key => $value) { ?>

 	<?php if($key == 'id_imdb') { ?>
 	<div class="uk-form-row">
       <label class="uk-form-label" for="form-s-it"><?php echo sanitizeStringForUrl($key); ?></label>
        <div class="uk-form-controls"> 
            <?php 
                $gt->field( array(
                	'type'          => 'text', 
                    'name'          => $plugin_meta.'-'.$key, 
                    'class'         => 'uk-form-medium uk-width-1-2',
                    'default'       => get_post_meta($post->ID, $plugin_meta.'-'.$key, true), 
                    'placeholder'   => 'Insert ID title IMDb '
                ));
            ?>
            <button type="button" id="fetch"> Fetch</button> <span id="loading"></span>
            <span id="result"></span>
    	</div>
     </div>
 	<?php } ?>
  
 	<?php if($key == 'content') { ?> <div class="uk-form-row"><button class="uk-button uk-button-primary"> Overviews </button></div> <?php } ?>
 	<?php if($key == 'line') { ?> <div class="uk-form-row"><hr></div> <?php } ?>

 	<?php if ($key != 'player' & $key != 'type_cta' & $key != 'locker' & $key != 'image' & $key != 'url_image' & $key != 'download:type_cta' & $key != 'download:locker' & $value != '' & $key != 'description'  & $key != 'image_actors' & $key != 'image_gallery') { ?>
    <div class="uk-form-row">
       <label class="uk-form-label" for="form-s-it"><?php echo sanitizeStringForUrl($key); ?></label>
        <div class="uk-form-controls"> 
            <?php 
                $gt->field( array(
                	'type'          => 'text', 
                    'name'          => $plugin_meta.'-'.$key, 
                    'class'         => 'uk-form-medium uk-width-3-4',
                    'default'       => get_post_meta($post->ID, $plugin_meta.'-'.$key, true), 
                    'placeholder'   => 'Enter '.sanitizeStringForUrl($key)
                ));
            ?>
            <?php if ($key == 'duration_show_offer') { echo '<div><i>if you need show offer CPA using timeout. *format milisecond ex:8000 = 8 second</i></div>'; } ?>
            <?php if ($key == 'url_referral') { echo '<div><i>insert url referral link CPA</i></div>'; } ?>
            <?php if ($key == 'url_cta') { echo '<div><i>insert url referral link CPA for the click play background cover image</i></div>'; } ?>
    	</div>
     </div> 
     <?php } ?>
     
     <?php if ($key == 'image_actors' || $key == 'image_gallery') { ?>
     <div class="uk-form-row">
       <label class="uk-form-label" for="form-s-it"><?php echo sanitizeStringForUrl($key); ?></label>
        <div class="uk-form-controls"> 
        <?php 
                $gt->field( array(
                    'type'          => 'textarea', 
                    'name'          => $plugin_meta.'-'.$key, 
                    'class'         => '',
                    'default'       => get_post_meta($post->ID, $plugin_meta.'-'.$key, true), 
                    'rows'          => 5,
                    'cols'          => 70,
                    'placeholder'   => ''
                ));
        ?>
        </div>
     </div>
     <?php } ?>

     <?php if ($key == 'description') { ?>
     <div class="uk-form-row">
       <label class="uk-form-label" for="form-s-it"><?php echo sanitizeStringForUrl($key); ?></label>
        <div class="uk-form-controls"> 
     	<?php 
                $gt->field( array(
                	'type'          => 'textarea', 
                    'name'          => $plugin_meta.'-'.$key, 
                    'class'         => '',
                    'default'       => get_post_meta($post->ID, $plugin_meta.'-'.$key, true), 
                    'rows'			=> 5,
					'cols'			=> 70,
                    'placeholder'   => ''
                ));
        ?>
     	</div>
     </div>
     <?php } ?>

     <?php if ($key == 'player') { ?>
     <div class="uk-form-row">
       <label class="uk-form-label" for="form-s-it"><?php echo sanitizeStringForUrl($key); ?></label>
        <div class="uk-form-controls"> 
            <?php 
                $gt->field( array(
                	'type'          => 'select', 
                    'name'          => $plugin_meta.'-'.$key, 
                    'class'         => 'uk-form-large uk-width-1-2',
                    'default'       => get_post_meta($post->ID, $plugin_meta.'-'.$key, true), 
                    'placeholder'   => '',
                    'options'       => array(
						''          	=> '---Please Select---',
						'youtube'  		=> 'Youtube',
						'mp4'    		=> 'MP4',
                        'openload'      => 'openload',
                     ),
                ));
            ?>
    	</div>
     </div>
     <?php } ?>

     <?php if ($key == 'url_image') { ?>
     <div class="uk-form-row">
       <label class="uk-form-label" for="form-s-it"><?php echo sanitizeStringForUrl($key); ?></label>
        <div class="uk-form-controls"> 
            <?php 
            	$gt->field( array(
					'type'			=> 'file', 
					'name'			=> $plugin_meta.'-'.$key, 
					'class' 		=> 'uk-form-medium',  
					'default'       => get_post_meta($post->ID, $plugin_meta.'-'.$key, true),
					'preview'       => get_post_meta($post->ID, $plugin_meta.'-'.$key, true), 
				) );
            ?>
    	</div>
     </div>
     <?php } ?>

     <?php if ($key == 'image') { ?>
     <div class="uk-form-row">
       <label class="uk-form-label" for="form-s-it"><?php echo sanitizeStringForUrl($key); ?></label>
        <div class="uk-form-controls"> 
            <?php 
            	$gt->field( array(
					'type'			=> 'file', 
					'name'			=> $plugin_meta.'-'.$key, 
					'class' 		=> 'uk-form-medium',  
					'default'       => get_post_meta($post->ID, $plugin_meta.'-'.$key, true),
					'preview'       => get_post_meta($post->ID, $plugin_meta.'-'.$key, true), 
				) );
            ?>
    	</div>
     </div>
     <?php } ?>

     <?php if ($key == 'type_cta') { ?>
     <div class="uk-form-row">
       <label class="uk-form-label" for="form-s-it"><?php echo sanitizeStringForUrl($key); ?></label>
        <div class="uk-form-controls"> 
            <?php 
                $gt->field( array(
                	'type'          => 'select', 
                    'name'          => $plugin_meta.'-'.$key, 
                    'class'         => 'uk-form-large uk-width-1-2',
                    'default'       => get_post_meta($post->ID, $plugin_meta.'-'.$key, true), 
                    'placeholder'   => '',
                    'options'       => array(
						''          	=> '---Please Select---',
						'openlink'  		=> 'Open Link',
						'openwindow'    	=> 'Open Window',
                        'opencenter'        => 'Open Center',
                        'openiframe'        => 'Open Iframe (recomended)'
                     ),
                ));
            ?>
    	</div>
     </div>
     <?php } ?>

     <?php if ($key == 'download:type_cta') { ?>
     <div class="uk-form-row">
       <label class="uk-form-label" for="form-s-it"><?php echo sanitizeStringForUrl($key); ?></label>
        <div class="uk-form-controls"> 
            <?php 
                $gt->field( array(
                	'type'          => 'select', 
                    'name'          => $plugin_meta.'-'.$key, 
                    'class'         => 'uk-form-large uk-width-1-2',
                    'default'       => get_post_meta($post->ID, $plugin_meta.'-'.$key, true), 
                    'placeholder'   => '',
                    'options'       => array(
						''          	=> '---Please Select---',
						'openlink'  		=> 'Open Link',
						'openwindow'    	=> 'Open Window'
                     ),
                ));
            ?>
    	</div>
     </div>
     <?php } ?>

     <?php if ($key == 'locker') { ?>
     <div class="uk-form-row">
       <label class="uk-form-label" for="form-s-it"><?php echo sanitizeStringForUrl($key); ?></label>
        <div class="uk-form-controls"> 
            <?php 

                $gt->field( array(
                	'type'          => 'radio', 
                    'name'          => $plugin_meta.'-'.$key,  
                    'default'       => get_post_meta($post->ID, $plugin_meta.'-'.$key, true),  
                    'options'       => array(
						'1'          	=> 'Active',
						'0'  			=> 'Non-Active', 
                     ),
                ));
            ?>
    	</div>
     </div>
     <?php } ?>

     <?php if ($key == 'download:locker') { ?>
     <div class="uk-form-row">
       <label class="uk-form-label" for="form-s-it"><?php echo sanitizeStringForUrl($key); ?></label>
        <div class="uk-form-controls"> 
            <?php 
                $gt->field( array(
                	'type'          => 'radio', 
                    'name'          => $plugin_meta.'-'.$key,  
                    'default'       => get_post_meta($post->ID, $plugin_meta.'-'.$key, true),  
                    'options'       => array(
						'1'          	=> 'Active',
						'0'  			=> 'Non-Active', 
                     ),
                ));
            ?>
    	</div>
     </div>
     <?php } ?>

 	<?php } ?>
 	</div>

    <?php
}

function sanitizeStringForUrl($string){
    $string = str_replace(array('_'),array(' '),$string);
    return ucwords(strtolower($string));
}

//now we are saving the data
function wpse_save_meta_fields( $post_id ) {

$options = get_option('wpviddycpa-option'); 
$custom_post_type = !empty($options['custome-post-type']) ? $options['custome-post-type'] : '';
$slug_custome_post =  !empty($options['slug-custome-post-type']) ? $options['slug-custome-post-type'] : ''; 

$plugin_meta = 'wpviddycpa-meta';

  // verify nonce
  if (!isset($_POST['wpse_our_nonce']) || !wp_verify_nonce($_POST['wpse_our_nonce'], basename(__FILE__)))
      return 'nonce not verified';

  // check autosave
  if ( wp_is_post_autosave( $post_id ) )
      return 'autosave';

  //check post revision
  if ( wp_is_post_revision( $post_id ) )
      return 'revision';

  if ( 'page' == $_POST['post_type'] ) {
    if ( !current_user_can( 'edit_page', $post_id ) )
    return;
  } else {
    if ( !current_user_can( 'edit_post', $post_id ) )
    return;
  }

  	foreach ( dataField() as $key => $value) { 

  		//if ($key != 'player' & $key != 'type_cta' & $key != 'locker' & $key != 'image' & $key != 'url_image' & $key != 'play' & $key != 'download') {
  			$field_name = $_POST['wpviddycpa-meta-'.$key];
  			update_post_meta( $post_id, 'wpviddycpa-meta-'.$key, $field_name );
  		//}
	}

}
add_action( 'save_post', 'wpse_save_meta_fields' );
add_action( 'new_to_publish', 'wpse_save_meta_fields' );

?>