<?php

/*
* Creating a function to create our CPT
*/

function cst_tv() {
     
    $custom_post_type = 'Tv Show';
    $slug_custome_post =  'tv';

    $slug_taxonomy_post =  'tvs'; 


// Set UI labels for Custom Post Type
    $labels = array(
        'name'                => _x( $custom_post_type, 'Post Type General Name', 'twentythirteen' ),
        'singular_name'       => _x( $custom_post_type, 'Post Type Singular Name', 'twentythirteen' ),
        'menu_name'           => __( $custom_post_type.'', 'twentythirteen' ),
        'parent_item_colon'   => __( 'Parent Movie', 'twentythirteen' ),
        'all_items'           => __( 'All Movies', 'twentythirteen' ),
        'view_item'           => __( 'View Movie', 'twentythirteen' ),
        'add_new_item'        => __( 'Add New '.$custom_post_type.' ', 'twentythirteen' ),
        'add_new'             => __( 'Add New', 'twentythirteen' ),
        'edit_item'           => __( 'Edit Movie', 'twentythirteen' ),
        'update_item'         => __( 'Update '. $custom_post_type.' ', 'twentythirteen' ),
        'search_items'        => __( 'Search Movie', 'twentythirteen' ),
        'not_found'           => __( 'Not Found', 'twentythirteen' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'twentythirteen' ),
    );
    
// Set other options for Custom Post Type
    
    $args = array( 
        'menu_icon'           => 'dashicons-format-video',
        'labels'              => $labels,
        // Features this CPT supports in Post Editor
        'supports'            => array( 'title', 'editor', 'thumbnail'), 
        // You can associate this CPT with a taxonomy or custom taxonomy. 
        //'taxonomies'          => array( $slug_taxonomy_post ),
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */   
        'public'              => true,
        //'publicly_queryable'  => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'query_var'          => true, 

        //'publicly_queryable'  => true,
        //'capability_type'     => 'page',
    );
    
    // Registering your Custom Post Type
    register_post_type( $slug_custome_post , $args );

}

/* Hook into the 'init' action so that the function
* Containing our post type registration is not 
* unnecessarily executed. 
*/

add_action( 'init', 'cst_tv', 0 );



// create two taxonomies, genres and writers for the post type "book"
function cst_tv_taxonomies() {
 
    $custom_post_type = 'Tv Category';
    $slug_custome_post =  'tv'; 

    $name_taxonomy_post =  'Tv Category';
    $slug_taxonomy_post =  'tvs'; 

    // Add new taxonomy, make it hierarchical (like categories)
    $labels = array(
        'name'              => _x( $name_taxonomy_post, 'taxonomy general name' ),
        'singular_name'     => _x( $name_taxonomy_post, 'taxonomy singular name' ),
        'search_items'      => __( 'Search '.$name_taxonomy_post ),
        'all_items'         => __( 'All '.$name_taxonomy_post ),
        'parent_item'       => __( 'Parent '.$name_taxonomy_post ),
        'parent_item_colon' => __( 'Parent '.$name_taxonomy_post.':' ),
        'edit_item'         => __( 'Edit '.$name_taxonomy_post ),
        'update_item'       => __( 'Update '.$name_taxonomy_post ),
        'add_new_item'      => __( 'Add New '.$name_taxonomy_post ),
        'new_item_name'     => __( 'New '.$name_taxonomy_post.' Name' ),
        'menu_name'         => __( $name_taxonomy_post ),
    );

    $args = array(
        'public'            => true,
        'hierarchical'      => true, 
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => $slug_taxonomy_post ),
    );

    register_taxonomy( $slug_taxonomy_post, array( $slug_custome_post ), $args );
}


// hook into the init action and call create_book_taxonomies when it fires
add_action( 'init', 'cst_tv_taxonomies', 0 );

 
?>