<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://www.linkedin.com/in/berbagimadani
 * @since      1.0.0
 * @package    wpviddycpa
 * @subpackage wpviddycpa/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    wpviddycpa
 * @subpackage wpviddycpa/includes
 * @author     berbagimadani@gmail.com
 */

$options = get_option('wpviddycpa-option');  
define('NAME_CUSTOME_POST', !empty($options['custome-post-type']) ? $options['custome-post-type'] : 'Lp Guardian' );
define('SLUG_CUSTOME_POST', !empty($options['slug-custome-post-type']) ? $options['slug-custome-post-type'] : 'lpguardian' );

define('NAME_TAXONOMY_POST', !empty($options['name-taxonomy-post-type']) ? $options['name-taxonomy-post-type'] : 'Type LpGuardian' );
define('SLUG_TAXONOMY_POST', !empty($options['slug-taxonomy-post-type']) ? $options['slug-taxonomy-post-type'] : 'type_guardian' );

class Wpviddycpa {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Apvideo_Curate_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $Apvideo_Curate    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */

	/**
	* Custome
	*/ 
	protected $plugin_slug;
	protected $plugin_option;
 

	public function __construct() {

		$this->plugin_name = '';
		$this->plugin_slug = 'wpviddycpa';
		$this->plugin_option = 'wpviddycpa-option';
		$this->version = '1.0.0';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		//$this->define_public_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Apvideo_Curate_Loader. Orchestrates the hooks of the plugin.
	 * - Apvideo_Curate_i18n. Defines internationalization functionality.
	 * - Apvideo_Curate_Admin. Defines all hooks for the admin area.
	 * - Apvideo_Curate_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-wpviddycpa-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-wpviddycpa-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-wpviddycpa-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		//require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-apvideo-curate-public.php';

		/**
		* TMDB 3
		*/
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/tmdb_v3/tmdb-api.php'; 
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/inc/custome_meta_box.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/inc/tv_custome_post_type.php'; // tv series
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/inc/walk.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/inc/yamm-nav-walker.php';
 
 
		$this->loader = new Wpviddycpa_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Apvideo_Curate_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Wpviddycpa_i18n();
		$plugin_i18n->set_domain( $this->get_plugin_name() );

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Wpviddycpa_Admin( $this->get_plugin_name(), $this->get_version(), $this->get_plugin_slug(), $this->get_plugin_option() );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );

	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Wpviddycpa_Public( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Apvideo_Curate_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}


	/**
	* Custome
	*/

	public function get_plugin_slug() {
		return $this->plugin_slug;
	}

	public function get_plugin_option() {
		return $this->plugin_option;
	}

}
