(function($) {
  //"use strict";

  $(function() {
    
    /* metabox scrape title IMDB*/

    $('#fetch').click(function(){
      	var data={}; 

    	/******* create blank page ******/
       	$('#loading').show().html('<span class="spinner is-active"></span>'); 
        $('#fetch').prop('disabled', true);

        data.id_imdb   = $('input[name="wpviddycpa-meta-id_imdb"]').val(); 

        data.action = "my_ajax";
       	jQuery.post(ajaxurl,data,function(response){
        //alert(response);
        $('#title-prompt-text').hide();
        $('input[name="post_title"]').val(response.title);
        $('input[name="wpviddycpa-meta-url_video"]').val(response.trailer); 
        
        $('input[name="wpviddycpa-meta-url_image"]').val(response.poster);  
        //$('.wpviddycpa-meta-url_image').html('<img src="'+response.poster+'" style="width:50%">');

        /* Asyncronus images */
        var img = $("<img />").attr('src', response.poster)
        .on('load', function() {
            if (!this.complete || typeof this.naturalWidth == "undefined" || this.naturalWidth == 0) {
                alert('broken image!');
            } else {
                $(".wpviddycpa-meta-url_image").append(img);
            }
        });

        $('select[name="wpviddycpa-meta-player"]').val('youtube');   

        $('input[name="wpviddycpa-meta-title"]').val(response.title);
        $('textarea[name="wpviddycpa-meta-description"]').val(response.overview);
        $('input[name="wpviddycpa-meta-rating"]').val(response.rating);
        $('input[name="wpviddycpa-meta-release"]').val(response.release);
 
        $('input[name="wpviddycpa-meta-image"]').val(response.image);  
        $('.wpviddycpa-meta-image').html('<img src="'+response.image+'" style="width:=30%">'); 

        $('input[name="wpviddycpa-meta-genres"]').val(response.genres);
        $('input[name="wpviddycpa-meta-actors"]').val(response.actors);
        $('.wp-editor-area').html(response.overview);

        $('#result').html(); 

        }).done(function() {
            $('#fetch').prop('disabled', false);
            $('#loading').slideUp( 300 ).delay( 200 ).fadeIn( 200 ).html('Done'); 
        }); 
     });    
    
    /* save dumy data category */

    $('#save-category').click(function(){
        var data={}; 

        /******* create blank page ******/
        $('#loading').show().html('<span class="spinner is-active"></span>');   

        data.category_custome_parent   = $('input[name="category_custome_parent"]').val();
        data.category_custome   = $('textarea[name="category_custome"]').val(); 
        data.cat_default  = $('textarea[name="cat_default"]').val(); 
        data.cat_tvs   = $('textarea[name="cat_tvs"]').val(); 
        data.action = "ajax_save_category";
        jQuery.post(ajaxurl,data,function(response){ 

            $('#result').html(response); 

        }).done(function() { 
            $('#loading').slideUp( 300 ).delay( 200 ).fadeIn( 200 ).html('Done'); 
        }); 
     });  

    /* scrape all tmdb movie*/

    $('#submit-scrape').click(function(){
        var data={};  

        /******* create blank page ******/
        $('#loading').show().html('<span class="spinner is-active"></span>'); 
        $('#submit-scrape').prop('disabled', true);

        data.type_movie   = $('#wpviddycpa-option-type').val();
        data.genres_field = $('#wpviddycpa-option-genres').val();
        data.page         = $('#wpviddycpa-option-page').val();
        data.category     = $('#wpviddycpa-option-category').val(); 
        data.action = "ajax_scrape_tmdb";
        jQuery.post(ajaxurl,data,function(response){
        
        var html = response;  

        $('#result-scrape').html(html);

        }).done(function() {
            $('#submit-scrape').prop('disabled', false);
            $('#loading').slideUp( 300 ).delay( 200 ).fadeIn( 200 ).html('Done'); 
        }); 

     });

    /* scrape top movie*/

    $('#submit-scrape-top-movie').click(function(){
        var data={};  

        /******* create blank page ******/
        $('#loading').show().html('<span class="spinner is-active"></span>'); 
        $('#submit-scrape-top-movie').prop('disabled', true);

        data.type_movie   = $('#wpviddycpa-option-type').val();
        data.genres_field = $('#wpviddycpa-option-genres').val();
        data.page         = $('#wpviddycpa-option-page').val();
        data.category     = $('#wpviddycpa-option-category').val(); 
        data.action = "ajax_scrape_tmdb_top_movie";
        jQuery.post(ajaxurl,data,function(response){
        
        var html = response;  
        
        $('#result-scrape').html(html);

        }).done(function() {
            $('#submit-scrape-top-movie').prop('disabled', false);
            $('#loading').slideUp( 300 ).delay( 200 ).fadeIn( 200 ).html('Done'); 
        }); 

     }); 

    /* scrape all tmdb TV series*/

    $('#submit-scrape-tv').click(function(){
        var data={};  

        /******* create blank page ******/
        $('#loading').show().html('<span class="spinner is-active"></span>'); 
        $('#submit-scrape').prop('disabled', true);

        data.type_movie   = $('#wpviddycpa-option-type').val();
        data.genres_field = $('#wpviddycpa-option-genres').val();
        data.page         = $('#wpviddycpa-option-page').val();
        data.category     = $('#wpviddycpa-option-category').val(); 
        data.action = "ajax_scrape_tv";
        jQuery.post(ajaxurl,data,function(response){
        
        var html = response;  

        $('#result-scrape').html(html); 

        }).done(function() {
            $('#submit-scrape').prop('disabled', false);
            $('#loading').slideUp( 300 ).delay( 200 ).fadeIn( 200 ).html('Done'); 
        }); 

     }); 

    /* save all to post */

    $('#save-all').click(function(){  
        var testElements = document.getElementsByClassName('save-one-post');
        var testDivs = Array.prototype.filter.call(testElements, function(testElement){
            testElement.click();
        });
    });    

    /* save one post */

    $('.save-one-post').live('click',function(){
        var data={};  
 
        /******* create blank page ******/ 
        var id  = $(this).attr('data');

        $(this).prop('disabled', true);

        data.title_movie    = $('#title'+id).attr('data'); 
        data.overview       = $('#overview'+id).attr('data'); 
        data.images_path    = $('#images'+id).attr('data'); 
        data.poster         = $('#poster'+id).attr('data'); 
        data.rating         = $('#rating'+id).attr('data'); 
        data.release_date   = $('#release_date'+id).attr('data'); 
        data.id_movie       = id; 
        data.category       = $('#wpviddycpa-option-category').val(); 
        data.action = "ajax_saved_one";
        jQuery.post(ajaxurl,data,function(response){
        
        $("#result").html(response);

        }).done(function() {
            $(this).prop('disabled', false); 
               $('.done'+id).html('Done'); 
        });  

    });    


  });



}(jQuery));
 
   