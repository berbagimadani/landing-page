<div class="">
    <div class="uk-grid">
        <?php if( isset($_GET['settings-updated']) ) { ?>
        <div class="uk-width-1-1">
        <div id="message" class="updated">
                    <p><strong><?php _e('Settings saved.') ?></strong></p>
        </div>
        </div>   
        <?php } ?>

         
        <div class="uk-width-3-10">
            <div class="uk-panel uk-panel-box">
                <h4 class="tm-article-subtitle">TV Series Database TMDB</h4>
                <?php 
                settings_fields( $plugin_option ); 
                $options = get_option( $plugin_option ); 
                ?>
                <form class="uk-form uk-form-stacked">
                    <div class="uk-form-row">
                        <label class="uk-form-label" for="form-s-it">Type</label>
                        <div class="uk-form-controls">
                            <?php 
                            $gt->field( array(
                                'type'          => 'select', 
                                'name'          => $plugin_option.'-type', 
                                'class'         => 'uk-width-1-1',
                                'default'       => !empty($options['type']) ? $options['type'] : '',
                                'options'       => array (  
                                    'tv'    => 'TV', 
                                ),
                            ));
                            ?>
                        </div>
                    </div>

                    <div class="uk-form-row">
                        <label class="uk-form-label" for="form-s-it">Genres</label>
                        <div class="uk-form-controls">
                            <?php 
                            $gt->field( array(
                                'type'          => 'select', 
                                'name'          => $plugin_option.'-genres', 
                                'class'         => 'uk-width-1-1',
                                'default'       => !empty($options['genres']) ? $options['genres'] : '',
                                'options'       => array (  
                                    '28'            => 'Action',
                                    '12'            => 'Adventure',
                                    '16'            => 'Animation',                   
                                    '35'            => 'Comedy',               
                                    '80'            => 'Crime',
                                    '99'            => 'Documentary', 
                                    '18'            => 'Drama',                   
                                    '10751'         => 'Family',
                                    '14'            => 'Fantasy',
                                    '36'            => 'History',                   
                                    '27'            => 'Horror',  
                                    '10402'         => 'Music', 
                                    '9648'          => 'Mystery',
                                    '10749'         => 'Romance',           
                                    '878'           => 'Sci-Fi', 
                                    '53'            => 'Thriller', 
                                    '10770'         => 'TV Movie',               
                                    '10752'         => 'War'
                                ),
                            ));
                            ?>
                        </div>
                    </div>

                    <div class="uk-form-row">
                        <label class="uk-form-label" for="form-s-it">From what page to start?</label>
                        <div class="uk-form-controls"> 
                            <?php 
                            $gt->field( array(
                                'type'          => 'text', 
                                'name'          => $plugin_option.'-page', 
                                'class'         => 'uk-width-1-1',
                                'default'       => !empty($options['text']) ? $options['text'] : 1, 
                                'placeholder'   => ''
                            ));
                            ?>
                        </div>
                    </div>
 
                    <hr>
                    <div class="uk-form-row">
                        <style type="text/css">
                        .uk-form select[multiple]{
                            height: 180px;
                        }
                        </style>
                        <label class="uk-form-label" for="form-s-it">Post To Category</label>
                        <div class="uk-form-controls">
                            <?php 
                            $gt->field( array(
                                'type'          => 'select', 
                                'name'          => $plugin_option.'-category', 
                                'class'         => 'uk-width-1-1',
                                'multiple'      => 'multiple',
                                'default'       => !empty($options['category']) ? $options['category'] : '',
                                'taxonomy'      => !empty($options['slug-taxonomy-post-type']) ? $options['slug-taxonomy-post-type'] : '', 
                            ));
                            ?>
                        </div>
                    </div>

                    <div class="uk-form-row">
                        <div class="uk-width-4-10">
                            <input type="button" class="uk-button button-primary" value="Scrape" id="submit-scrape-tv"> <span id="loading"></span>
                        </div>
                    </div>
               </form>
            </div>
        </div>
         <div class="uk-width-7-10">
            <div class="">
                <input type="button" class="uk-button uk-button-success" value="Save All" id="save-all"> <span id="loading-all"></span>
                <table class="uk-table uk-table-striped">
                    <tr>
                        <th>All TV Series</th> 
                    </tr>

                    <tr id="result-scrape"></tr>
                    
                </table>
            </div>
        </div>

        <div class="uk-width-1-1"> 
            <div id="result"></div> 
        </div>

       

    </div>
</div>