<div class="">
    <div class="uk-grid">
        <?php if( isset($_GET['settings-updated']) ) { ?>
        <div class="uk-width-1-1">
        <div id="message" class="updated">
                    <p><strong><?php _e('Settings saved. please update permalinks click Settings -> permalinks choose post name and Save Change') ?></strong></p>
        </div>
        </div>
        <?php } ?>

        <div class="uk-width-7-10">
            <div class="uk-panel uk-panel-box">
                <?php
                $options_key = get_option('wpviddycpaattributes'); 
                ?>
                
                <h4 class="tm-article-subtitle"><b>Dump Databse Category</b></h4> 
                <?php if(!empty($options_key['attributes'])) :?>

                    <div class="uk-form-row">
                        <label class="uk-form-label" for="form-s-it">Category</label>
                        <div class="uk-form-controls"> 
                            <?php 
                            $gt->field( array(
                                'type'          => 'text', 
                                'name'          => 'category_custome_parent', 
                                'class'         => 'uk-form-large uk-width-1-2',
                                'default'       => !empty($category_custome_parent) ? $category_custome_parent : 'Genres',  
                                'placeholder'   => 'Please enter the category Parent'
                            ));
                            ?> 
                        </div> 
                    </div>  
                    <div class="uk-form-row">
                        <?php
                        $xCat = new  Wpviddycpa_Field();
                        $categories = $xCat->get_taxonomy('category');

                       

                            $cats = '
Action;
Adventure;
Animation;
Biography;                   
Comedy;               
Crime;
Documentary; 
Drama;                   
Family;
Fantasy;
History;                   
Horror;  
Music; 
Mystery;           
Sci-Fi; 
Thriller; 
TV Show;               
War;';


if(!empty($categories)){
    $cats = null;
}
 

                        ?>
                        <label class="uk-form-label" for="form-s-it">Categories genres</label>
                        <div class="uk-form-controls"> 
                            <?php 
                            $gt->field( array(
                                'type'          => 'textarea', 
                                'name'          => 'category_custome', 
                                'class'         => '',
                                'default'       => $cats, 
                                'rows'          => 5,
                                'cols'          => 70,
                                'placeholder'   => 'Please enter the category child'
                            ));
                            ?> 
                        </div>
                        <div><i><small>Added category genres with separated comma ;</small></i></div>
                    </div>  

                    <div class="uk-form-row">
                        <?php
                            $cat_default = '
Top Rated;
Upcoming;
Now Playing;';

if(!empty($categories)){
     $cat_default = null;
}

                        ?>
                        <label class="uk-form-label" for="form-s-it">Category Default</label>
                        <div class="uk-form-controls"> 
                            <?php 
                            $gt->field( array(
                                'type'          => 'textarea', 
                                'name'          => 'cat_default', 
                                'class'         => '',
                                'default'       => $cat_default, 
                                'rows'          => 5,
                                'cols'          => 70,
                                'placeholder'   => 'Please enter the category Default'
                            ));
                            ?> 
                        </div>
                        <div><i><small>Added category default with separated comma ;</small></i></div> 
                    </div>  


                    <div class="uk-form-row">
                        <?php
                            $cat_tvs = '
TV shows Airing;
On The Air;
Popular TV Series;';

 $category_tv = $xCat->get_taxonomy('tvs');
 if(!empty($category_tv)){
     $cat_tvs = null;
}

                        ?>
                        <label class="uk-form-label" for="form-s-it">Category TV Show</label>
                        <div class="uk-form-controls"> 
                            <?php 
                            $gt->field( array(
                                'type'          => 'textarea', 
                                'name'          => 'cat_tvs', 
                                'class'         => '',
                                'default'       => $cat_tvs, 
                                'rows'          => 5,
                                'cols'          => 70,
                                'placeholder'   => 'Please enter the category TV show'
                            ));
                            ?> 
                        </div>
                        <div><i><small>Added category TV Show with separated comma ;</small></i></div>
                    </div>  

                    <div class="uk-form-row">
                        <div class="uk-width-2-10">
                        <input type="submit" id="save-category" class="uk-button button-primary" value="Save"> <span id="loading"></span>
                        </div>
                    </div>   
                    <?php endif; ?>
             </div> 


            
               <form method="post" action="options.php" class="uk-form-stacked">
                <?php
                settings_fields( $plugin_option ); 
                $options = get_option( $plugin_option ); 
                ?>   
                    

            

            <!-- 
                *
                * GENERAL STYLES SETTING
                *
             -->

            <div class="uk-panel uk-panel-box" style="background:#ccc">
                <h4 class="tm-article-subtitle"><b>Setting General</b></h4>
                    
                    <?php if(!empty($options_key['attributes'])) :?>

                    <div class="uk-form-row">
                        <label class="uk-form-label" for="form-s-it">Intro Movie Production ( Youtube ID )</label>
                        <div class="uk-form-controls">
<?php
$intro = '
CK8jAE0lM0w;
8FuoHDmTms4; 
eBWJVreSHzY;
Y8Awdb5Xr4k;
Y23_U-akcMs;
VOjqnb3aICM;';
?>
                            <?php 
                            $gt->field( array( 
                                'type'          => 'textarea',  
                                'name'          =>  $plugin_option.'[general-intro-video]', 
                                'class'         => '',  
                                'default'       => !empty($options['general-intro-video']) ? $options['general-intro-video'] : $intro, 
                                'rows'          => 5,
                                'cols'          => 100
                            ));
                            ?>
                        </div>
                        <small>Enter row for multiple entry with separeted comma</small>
                    </div>

                    <h4 class="tm-article-subtitle"><b>General URL link CPA Network</b></h4> 
                    <div class="uk-form-row">
                        <label class="uk-form-label" for="form-s-it">Enter url link CPA network</label>
                        <div class="uk-form-controls"> 
                            <?php 
                            $gt->field( array( 
                                'type'          => 'textarea',  
                                'name'          =>  $plugin_option.'[general-url-cpa]', 
                                'class'         => '',  
                                'default'       => !empty($options['general-url-cpa']) ? $options['general-url-cpa'] : '', 
                                'rows'          => 5,
                                'cols'          => 100
                            ));
                            ?>
                        </div>
                        <small>Enter row for multiple entry with separeted comma, url link cpa display random every movies</small>
                    </div>
  
                    <div class="uk-form-row">
                        <label class="uk-form-label" for="form-s-it">Copyright</label>
                        <div class="uk-form-controls"> 
                            <?php 
                            $gt->field( array(
                                'type'          => 'text', 
                                'name'          => $plugin_option.'[copyright]', 
                                'class'         => 'uk-form-large uk-width-1-1',
                                'default'       => !empty($options['copyright']) ? $options['copyright'] : '', 
                                'placeholder'   => 'Enter Copyright'
                            ));
                            ?>
                        </div>
                    </div> 
                    
                     
                    <div class="uk-form-row">
                        <label class="uk-form-label" for="form-s-it">Show Title Offer Popup</label>
                        <div class="uk-form-controls"> 
                            <?php 
                            $gt->field( array(
                                'type'          => 'text', 
                                'name'          => $plugin_option.'[show-title-offer]', 
                                'class'         => 'uk-form-large uk-width-1-1',
                                'default'       => !empty($options['show-title-offer']) ? $options['show-title-offer'] : '', 
                                'placeholder'   => 'Show message text On appear Ads Popup'
                            ));
                            ?>
                        </div>
                    </div>
                    
                    <div class="uk-form-row">
                        <label class="uk-form-label" for="form-s-it">Logo Header</label>
                        <div class="uk-form-controls"> 
                            <?php 
                            $gt->field( array( 
                                'type'          => 'file', 
                                'name'          =>  $plugin_option.'[logo]', 
                                'class'         => 'uk-form-medium',  
                                'default'       => !empty($options['logo']) ? $options['logo'] : '', 
                                'preview'       => !empty($options['logo']) ? $options['logo'] : '', 

                            ));
                            ?>
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label" for="form-s-it">Favicon (.icon (32x32 px))</label>
                        <div class="uk-form-controls"> 
                            <?php 
                            $gt->field( array( 
                                'type'          => 'file', 
                                'name'          =>  $plugin_option.'[favicon]', 
                                'class'         => 'uk-form-medium',  
                                'default'       => !empty($options['favicon']) ? $options['favicon'] : '', 
                                'preview'       => !empty($options['favicon']) ? $options['favicon'] : '', 

                            ));
                            ?>
                        </div>
                    </div> 
                    <h4 class="tm-article-subtitle"><b>SEO</b></h4> 

                    <div class="uk-form-row">
                        <label class="uk-form-label" for="form-s-it">Javascrtip code for google analytics, facebook pixel, popcash, etc</label>
                        <div class="uk-form-controls"> 
                            <?php 
                            $gt->field( array( 
                                'type'          => 'textarea',  
                                'name'          =>  $plugin_option.'[ads-scripts]', 
                                'class'         => '',  
                                'default'       => !empty($options['ads-scripts']) ? $options['ads-scripts'] : '', 
                                'rows'          => 5,
                                'cols'          => 70
                            ));
                            ?>
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label" for="form-s-it">Meta Keyword</label>
                        <div class="uk-form-controls"> 
                            <?php 
                            $gt->field( array( 
                                'type'          => 'textarea', 
                                'name'          =>  $plugin_option.'[meta-keyword]', 
                                'class'         => '',  
                                'default'       => !empty($options['meta-keyword']) ? $options['meta-keyword'] : '', 
                                'rows'          => 3,
                                'cols'          => 70
                            ));
                            ?>
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label" for="form-s-it">Meta Description</label>
                        <div class="uk-form-controls"> 
                            <?php 
                            $gt->field( array( 
                                'type'          => 'textarea', 
                                'name'          =>  $plugin_option.'[meta-description]', 
                                'class'         => '',  
                                'default'       => !empty($options['meta-description']) ? $options['meta-description'] : '', 
                                'rows'          => 3,
                                'cols'          => 70
                            ));
                            ?>
                        </div>
                    </div>

<!-- 
    *
    * THEMES STYLES
    *
 --> 
                
                <h4 class="tm-article-subtitle"><b>Themes Style</b></h4> 

                    <div class="uk-form-row uk-form-horizontal">
                        <label class="uk-form-label" for="form-s-it">Background Body</label>
                        <div class="uk-form-controls"> 
                            <div class="uk-width-2-10">
                            <?php 
                            $gt->field( array( 
                                'type'          => 'colorpicker',  
                                'name'          =>  $plugin_option.'[background-body]',  
                                'default'       => !empty($options['background-body']) ? $options['background-body'] : '', 
                            ));
                            ?>
                            </div>
                        </div>
                        <label class="uk-form-label" for="form-s-it">Background Top Header</label>
                        <div class="uk-form-controls"> 
                            <div class="uk-width-2-10">
                            <?php 
                            $gt->field( array( 
                                'type'          => 'colorpicker',  
                                'name'          =>  $plugin_option.'[background-top-header]',  
                                'default'       => !empty($options['background-top-header']) ? $options['background-top-header'] : '', 
                            ));
                            ?>
                            </div>
                        </div>
                        <label class="uk-form-label" for="form-s-it">Color Top Header</label>
                        <div class="uk-form-controls"> 
                            <div class="uk-width-2-10">
                            <?php 
                            $gt->field( array( 
                                'type'          => 'colorpicker',  
                                'name'          =>  $plugin_option.'[color-top-header]',  
                                'default'       => !empty($options['color-top-header']) ? $options['color-top-header'] : '', 
                            ));
                            ?>
                            </div>
                        </div>
                        <label class="uk-form-label" for="form-s-it">Color Top Header :Hover</label>
                        <div class="uk-form-controls"> 
                            <div class="uk-width-2-10">
                            <?php 
                            $gt->field( array( 
                                'type'          => 'colorpicker',  
                                'name'          =>  $plugin_option.'[color-top-header-hover]',  
                                'default'       => !empty($options['color-top-header-hover']) ? $options['color-top-header-hover'] : '', 
                            ));
                            ?>
                            </div>
                        </div>
                        <label class="uk-form-label" for="form-s-it">Button Search Bacgkround</label>
                        <div class="uk-form-controls"> 
                            <div class="uk-width-2-10">
                            <?php 
                            $gt->field( array( 
                                'type'          => 'colorpicker',  
                                'name'          =>  $plugin_option.'[background-search]',  
                                'default'       => !empty($options['background-search']) ? $options['background-search'] : '', 
                            ));
                            ?>
                            </div>
                        </div>
                        <label class="uk-form-label" for="form-s-it">Bacgkround Label Category</label>
                        <div class="uk-form-controls"> 
                            <div class="uk-width-2-10">
                            <?php 
                            $gt->field( array( 
                                'type'          => 'colorpicker',  
                                'name'          =>  $plugin_option.'[background-heading]',  
                                'default'       => !empty($options['background-heading']) ? $options['background-heading'] : '', 
                            ));
                            ?>
                            </div>
                        </div>
                    </div>

                    <div class="uk-form-row uk-form-horizontal">
                        <label class="uk-form-label" for="form-s-it">Background Footer</label>
                        <div class="uk-form-controls"> 
                            <div class="uk-width-2-10">
                            <?php 
                            $gt->field( array( 
                                'type'          => 'colorpicker',  
                                'name'          =>  $plugin_option.'[background-footer]',  
                                'default'       => !empty($options['background-footer']) ? $options['background-footer'] : '', 
                            ));
                            ?>
                            </div>
                        </div>
                        <label class="uk-form-label" for="form-s-it">Font Color Footer</label>
                        <div class="uk-form-controls"> 
                            <div class="uk-width-2-10">
                            <?php 
                            $gt->field( array( 
                                'type'          => 'colorpicker',  
                                'name'          =>  $plugin_option.'[font-color-footer]',  
                                'default'       => !empty($options['font-color-footer']) ? $options['font-color-footer'] : '', 
                            ));
                            ?>
                            </div>
                        </div>
                    </div>

                    <div class="uk-form-row uk-form-horizontal">
                        <label class="uk-form-label" for="form-s-it">Background Single Content</label>
                        <div class="uk-form-controls"> 
                            <div class="uk-width-2-10">
                            <?php 
                            $gt->field( array( 
                                'type'          => 'colorpicker',  
                                'name'          =>  $plugin_option.'[background-single-content]',  
                                'default'       => !empty($options['background-single-content']) ? $options['background-single-content'] : '', 
                            ));
                            ?>
                            </div>
                        </div>
                        <label class="uk-form-label" for="form-s-it">Font Color Single Content</label>
                        <div class="uk-form-controls"> 
                            <div class="uk-width-2-10">
                            <?php 
                            $gt->field( array( 
                                'type'          => 'colorpicker',  
                                'name'          =>  $plugin_option.'[font-color-single-content]',  
                                'default'       => !empty($options['font-color-single-content']) ? $options['font-color-single-content'] : '', 
                            ));
                            ?>
                            </div>
                        </div>
                        <label class="uk-form-label" for="form-s-it">Background Color Tab</label>
                        <div class="uk-form-controls"> 
                            <div class="uk-width-2-10">
                            <?php 
                            $gt->field( array( 
                                'type'          => 'colorpicker',  
                                'name'          =>  $plugin_option.'[background-tab]',  
                                'default'       => !empty($options['background-tab']) ? $options['background-tab'] : '', 
                            ));
                            ?>
                            </div>
                        </div>
                    </div>
 


                    <div class="uk-form-row">
                        <div class="uk-width-2-10">
                        <input type="submit" class="uk-button button-primary" value="Save"> <span id="loading"></span>
                        </div>
                    </div>
                <?php endif; ?> 
               </form>   
            </div>

            <?php if(empty($options_key['attributes'])) :?>
                 <label class="uk-form-label" for="form-s-it" style="color:red">Please Klil Activated and insert the code token</label>
            <?php endif; ?> 
        </div>


        
        <div class="uk-width-3-10">
            <div class="uk-panel uk-panel-box">
                <h4 class="tm-article-subtitle"><b>Support</b></h4>  
                <h5>
                    <a href="https://www.facebook.com/groups/amazing.tools/?ref=bookmarks"><button class="uk-button uk-button-primary">Join Group Facebook</button></a>
                    <a href="https://www.facebook.com/yellowalmeshra"><i class="fa fa-facebook"></i> <button class="uk-button uk-button-primary">Add Me</button> </a>
                </h5> 
            </div>

             
            <div class="uk-panel uk-panel-box">
                <h4 class="tm-article-subtitle"><b>UPDATE</b></h4>  
                
            </div>

        </div>

        <div class="uk-width-1-1">
            <div class="uk-panel uk-panel-box">
                <div id="result"></div>
            </div>
        </div>

    </div>
</div>