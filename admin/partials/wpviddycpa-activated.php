<div class="">
    <div class="uk-grid">
        <?php if( isset($_GET['settings-updated']) ) { ?>
        <div class="uk-width-1-1">
        <div id="message" class="updated">
                    <p><strong><?php _e('Settings saved.') ?></strong></p>
        </div>
        </div>   
        <?php } ?>

        <div class="uk-width-7-10">
            <div class="uk-panel uk-panel-box">
                <h4 class="tm-article-subtitle"><b>Enter The Token</b></h4>
               <form method="post" action="options.php" class="uk-form-stacked">
                <?php
                settings_fields('wpviddycpaattributes'); 
                $options_key = get_option('wpviddycpaattributes');  
                ?>
                    <style type="text/css">
                    input[value].style-token{
                        background: #ce0c0c;
                        font-size: 18px;
                        color: #fff;
                        font-weight: bold; 
                    }
                    
                    </style>
                    <div class="uk-form-row">
                        <label class="uk-form-label" for="form-s-it">Insert Code Token For The Activation</label>
                        <div class="uk-form-controls"> 
                            <?php 
                            $gt->field( array(
                                'type'          => 'text', 
                                'name'          => 'wpviddycpaattributes[attributes]', 
                                'class'         => 'uk-form-large uk-width-1-1 style-token',
                                'default'       => !empty($options_key['attributes']) ? $options_key['attributes'] : '', 
                                'placeholder'   => 'Please enter the token obtained from your purchase'
                            ));
                            ?>
                            <?php 
                            $gt->field( array(
                                'type'          => 'text', 
                                'name'          => 'wpviddycpaattributes[attributes2]', 
                                'class'         => 'uk-hidden',
                                'default'       => $_SERVER['SERVER_NAME'], 
                                'placeholder'   => ''
                            ));
                            ?>
                        </div>
                        <div><i>Code token has been delivery in your inbox email order</i></div>
                    </div>  

                    <hr>
 

                    <div class="uk-form-row">
                        <div class="uk-width-2-10">
                        <input type="submit" class="uk-button button-primary" value="SAVE"> 
                        </div>
                    </div>
               </form>
            </div>

            
          
    </div>


    </div>
</div>