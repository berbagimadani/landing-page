<div class="uk-margin-top">
    <div class="uk-grid">

<!-- Top Filter -->
        <div class="uk-width-3-10">
            <div class="uk-panel uk-panel-box"> 
                 <h4 class="tm-article-subtitle">Top Movie Database TMDB</h4>
                <?php 
                settings_fields( $plugin_option ); 
                $options = get_option( $plugin_option ); 
                ?>
                <form class="uk-form uk-form-stacked">
                    <div class="uk-form-row">
                        <label class="uk-form-label" for="form-s-it">Type</label>
                        <div class="uk-form-controls">
                            <?php 
                            $gt->field( array(
                                'type'          => 'select', 
                                'name'          => $plugin_option.'-type', 
                                'class'         => 'uk-form-large uk-width-1-1',
                                'default'       => !empty($options['type']) ? $options['type'] : '',
                                'options'       => array ( 
                                        'movie'  => 'Movie',
                                        //'tv'    => 'TV', 
                                ),
                            ));
                            ?>
                        </div>
                    </div>

                    <div class="uk-form-row">
                        <label class="uk-form-label" for="form-s-it">Category</label>
                        <div class="uk-form-controls">
                            <?php 

                            $category2 = get_term_by('name', 'Now Playing', 'category'); 
                            $now_playing = $category2->term_id;

                            $category3 = get_term_by('name', 'Top Rated', 'category'); 
                            $top_rated = $category3->term_id;

                            $category4 = get_term_by('name', 'Upcoming', 'category'); 
                            $upcoming = $category4->term_id; 

                            $gt->field( array(
                                'type'          => 'select', 
                                'name'          => $plugin_option.'-category', 
                                'class'         => 'uk-form-large uk-width-1-1',
                                'default'       => '',
                                'options'       => array (  
                                    $now_playing         => 'Now Playing',
                                    $top_rated           => 'Top Rated',
                                    $upcoming            => 'Upcoming',       
                                ),
                            ));
                            ?>
                        </div>
                    </div>
                    <?php
                     

                    $args = array( 
                        'orderby'            => 'name',
                        'order'              => 'DESC', 
                        'show_count'         => 1,
                        'hide_empty'         => 0, 
                        'category'             => '',
                        'taxonomy'           => 'category', 
                    ); 

                    $opts = null; 
                    $categories = get_categories( $args );
                    if ( $categories ) {
                        foreach ($categories as $key => $value) {
                            //echo $value->name.'<br>';
                        }
                    }

                    /*
                    $term_id = $category2->term_id;
                    $taxonomy_name = 'category';
                    $termchildren = get_term_children( $term_id, $taxonomy_name );

                    echo '<ul>';
                    foreach ( $termchildren as $child ) {
                        $term = get_term_by( 'id', $child, $taxonomy_name );
                        echo '<li><a href="' . get_term_link( $child, $taxonomy_name ) . '">' . $term->name . '</a></li>';
                    }
                    echo '</ul>';*/

                    ?>


                    <div class="uk-form-row">
                        <label class="uk-form-label" for="form-s-it">From what page to start?</label>
                        <div class="uk-form-controls"> 
                            <?php 
                            $gt->field( array(
                                'type'          => 'text', 
                                'name'          => $plugin_option.'-page', 
                                'class'         => 'uk-form-large uk-width-1-2',
                                'default'       => !empty($options['text']) ? $options['text'] : 1, 
                                'placeholder'   => ''
                            ));
                            ?>
                        </div>
                    </div> 



                    <div class="uk-form-row">
                        <div class="uk-width-4-10">
                        <input type="button" class="uk-button button-primary" value="Scrape" id="submit-scrape-top-movie"> <span id="loading"></span>
                        </div>
                    </div>
               </form>
            </div>
        </div>
         <div class="uk-width-7-10">
            <div class="uk-panel uk-panel-box">
                <input type="button" class="uk-button uk-button-success" value="Save All" id="save-all"> <span id="loading-all"></span>
                <table class="uk-table uk-table-striped">
                    <tr>
                        <th>Movies By Filter</th> 
                    </tr>

                    <tr id="result-scrape"></tr>
                    
                </table>
            </div>
        </div>
        <div class="uk-width-1-1"> 
            <div id="result"></div> 
        </div>
        <!-- End Top Filter -->
    
    </div>
</div>