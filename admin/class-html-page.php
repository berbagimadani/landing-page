
<?php

class Html_Page {
    
    /**
    * Render php html input
    */
    public function render_page_one( $plugin_option, $file ) {

    	require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/inc/class-wpviddycpa-field.php'; 
		$gt = new Wpviddycpa_Field(); 

        $options = get_option( $plugin_option );
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/'.$file.'.php';
    }

}

?>
