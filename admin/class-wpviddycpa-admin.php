<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://www.linkedin.com/in/berbagimadani
 * @since      1.0.0
 * @package    wpviddycpa
 * @subpackage wpviddycpa/includes
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    wpviddycpa
 * @subpackage wpviddycpa/includes
 * @author     berbagimadani@gmail.com
 */
class Wpviddycpa_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */

	/**
	* Custome
	*/ 
	private $plugin_slug;
	private $plugin_option;

	protected $plugin_screen_hook = array(); 


	public function __construct( $plugin_name, $version, $plugin_slug, $plugin_option ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

		$this->plugin_slug = $plugin_slug;
		$this->plugin_option = $plugin_option;  

		// register actions
		add_action( 'admin_menu', array( $this, 'wpviddycpa_admin_menu' ) );

		/* Save OPtion */
		add_action( 'admin_init', array( $this, 'register_wpviddycpa' ) );
		add_action( 'admin_notices', array( $this, 'unique_identifyer_admin_notices' ) );


		//add_action( 'admin_print_scripts', array( $this, 'admin_inline_js' ) );

		add_action( 'wp_ajax_ajax_saved_one', array( $this, 'ajax_saved_one') );
		add_action( 'wp_ajax_nopriv_ch_post_action', array( $this, 'ajax_saved_one') );

		add_action( 'wp_ajax_ajax_scrape_tv', array( $this, 'ajax_scrape_tv') );
		add_action( 'wp_ajax_ajax_scrape_tmdb', array( $this, 'ajax_scrape_tmdb') );
		add_action( 'wp_ajax_ajax_scrape_tmdb_top_movie', array( $this, 'ajax_scrape_tmdb_top_movie') );
		add_action( 'wp_ajax_ajax_save_category', array( $this, 'ajax_save_category') );
		add_action( 'wp_ajax_my_ajax', array( $this, 'my_ajax') ); //  scrape imdb id

		add_action( 'wp_ajax_nopriv_ch_post_action', array( $this, 'my_ajax') ); // scrape imdb id
		add_action( 'wp_ajax_nopriv_ch_post_action', array( $this, 'ajax_save_category') );
		add_action( 'wp_ajax_nopriv_ch_post_action', array( $this, 'ajax_scrape_tmdb') );
		add_action( 'wp_ajax_nopriv_ch_post_action', array( $this, 'ajax_scrape_tmdb_top_movie') );
		add_action( 'wp_ajax_nopriv_ch_post_action', array( $this, 'ajax_scrape_tv') );
		
		

		//add_action( 'init', array( $this, 'init' ) ); 
		register_nav_menus( array(
			'lpguardian-menu' => 'Landing Page Menu', 
		) );
 
	}
 	
 	public function ajax_saved_one(){
 		ob_clean();
 		//$options = get_option( $this->plugin_option );
 		$tmdb = new TMDB(); 
		$tmdb->setAPIKey('78e24d6186c66d3c607687d177694bd4');
    	
 		$id 		=  $_POST['id_movie'];
 		$title 		=  $_POST['title_movie'];
 		$overview 	=  $_POST['overview'];
 		$images 	=  $_POST['images_path'];
 		$poster 	=  $_POST['poster'];
 		$rating 	=  $_POST['rating'];
 		$release_date 	=  $_POST['release_date'];
 		$category = $_POST['category']; 

    	$single = $tmdb->getMovie($id);

 		$genres = $single->getGenres();
       	$cast = $single->get('credits');
		$gen = null;
	    $act = null;
		foreach ($genres as $key => $value) {
            $gen .= $value->getName().',';
        }
        for ($i=0; $i < 10 ; $i++) { 
        	$act .= @$cast['cast'][$i]['name'].','; 
        }
 
	    $my_post = array(
			'post_title'    => wp_strip_all_tags( $title ),
			'post_content'  => $overview,
			'post_status'   => 'publish', 
			'post_type'		=> 'post'
		);			 
		$post_id = wp_insert_post( $my_post ); // Insert the post into the database

		$genres = $single->getGenres();
       	$cast = $single->get('credits');
		$gen = null;
	    $actors = null;
	    $data_actors = null; 
		foreach ($genres as $key => $value) {
            $gen .= $value->getName().',';
        }

        /* actors */
        $max_cast = sizeof($cast['cast']);
        for ($i=0; $i < $max_cast; $i++) { 
        	$actors[]= @$cast['cast'][$i]['name']; 

        	if(!empty(@$cast['cast'][$i]['profile_path'])){

        		$data_actors[]['datas'] = array( 'name' => @$cast['cast'][$i]['name'], 'profile_path' => @$cast['cast'][$i]['profile_path']) ;  
        	}

        } 
        $act = implode(',', $actors);  
        /* end actors */

        /* gallery images */
        $images_gallery = $single->get('images');
        $file_gallery=null;
        for ($y=0; $y<15; $y++) {
        	if(!empty(@$images_gallery['posters'][$y]['file_path'])) { 
        		$file_gallery[] = @$images_gallery['posters'][$y]['file_path'];
        	}
        }
        /* end gallery images */ 
        
		add_post_meta( $post_id, 'wpviddycpa-meta-url_video', $single->getTrailer() );
		add_post_meta( $post_id, 'wpviddycpa-meta-url_image', $poster ); 
		
		add_post_meta( $post_id, 'wpviddycpa-meta-image', $images );
		add_post_meta( $post_id, 'wpviddycpa-meta-rating', $rating );
		add_post_meta( $post_id, 'wpviddycpa-meta-actors', $act );
		add_post_meta( $post_id, 'wpviddycpa-meta-image_actors', json_encode($data_actors) );
		add_post_meta( $post_id, 'wpviddycpa-meta-image_gallery', json_encode($file_gallery) );
		add_post_meta( $post_id, 'wpviddycpa-meta-genres', $gen );
		add_post_meta( $post_id, 'wpviddycpa-meta-release', $release_date ); 
		add_post_meta( $post_id, 'wpviddycpa-meta-duration', $single->get('runtime'));  

		$taxonomy = 'category';
		wp_set_post_terms( $post_id, $category, $taxonomy );

	    wp_die(); 
 	}

	public function ajax_scrape_tmdb() {
		ob_clean();
		$options = get_option( $this->plugin_option );
		$tmdb = new TMDB(); 
		$tmdb->setAPIKey('78e24d6186c66d3c607687d177694bd4');
    	
    	$single = $tmdb->getMovie(271110);

		$type_movie = $_POST['type_movie'];
		$genres_field = $_POST['genres_field'];
		$page = $_POST['page'];
		$category = $_POST['category']; 
		
		$movies = $tmdb->getDiscoverMovies( $page, $genres_field );

		$tmdb_url = $tmdb->getImageURL('w185');
		foreach($movies as $movie){
 			//$single = $tmdb->getMovie($movie->getID());
 			//echo $single->getTrailer();
 			
 			/*$backdrop_path = $single->get('backdrop_path'); 
 			$images = 'http://image.tmdb.org/t/p/w1280'.$backdrop_path;
 			if(empty($backdrop_path)) {
 				$images = 'http://image.tmdb.org/t/p/original'.$movie->getPoster();
 			}
	    	echo $single->getTitle().'<img src="'.$images.'" width="300"><br>';*/

	    	echo '<tr>
	    		<td id="title'.$movie->getID().'" data="'.$movie->getTitle().'">'.$movie->getTitle().'<td>
	    		<td>
	    		<span id="overview'.$movie->getID().'" data="'.$movie->get('overview').'"></span>
	    		<span id="images'.$movie->getID().'" data="'.$tmdb_url.$movie->get('poster_path').'"><img src='.$tmdb_url.$movie->get('poster_path').' width="100"></span>
	    		<span id="poster'.$movie->getID().'" data="http://image.tmdb.org/t/p/w1280'.$movie->get('backdrop_path').'"></span>
	    		<span id="rating'.$movie->getID().'" data="'.$movie->get('vote_average').'"></span>
	    		<span id="release_date'.$movie->getID().'" data="'.$movie->get('release_date').'"></span>
	    		</td>
	    		<td><input type="button" class="uk-button uk-button-success uk-button-small save-one-post" data="'.$movie->getID().'" value="Save"><span class="done'.$movie->getID().'"></span><td></tr>';
	    }

		//wp_send_json($childs);
	    wp_die(); 
	}

	/*
		Scrape top rated movie, now playing, upcoming
	*/
	public function ajax_scrape_tmdb_top_movie() {
		ob_clean();
		$options = get_option( $this->plugin_option );
		$tmdb = new TMDB(); 
		$tmdb->setAPIKey('78e24d6186c66d3c607687d177694bd4');
    	
    	$single = $tmdb->getMovie(271110);

		$type_movie = $_POST['type_movie'];
		$genres_field = $_POST['genres_field'];
		$page = $_POST['page'];
		$category = $_POST['category']; 
		
		 
		$result = get_term_by('id', $category, 'category');
        
        if( $result->name == 'Now Playing' ) {
			$movies = $tmdb->getNowPlayingMovies( $page );
		}
		if( $result->name == 'Top Rated' ) {
			$movies = $tmdb->getTopRatedMovies( $page );
		}
		if( $result->name == 'Upcoming' ) {
			$movies = $tmdb->getUpcomingMovies( $page );
		} 

		$tmdb_url = $tmdb->getImageURL('w185');
		foreach($movies as $movie){
 			 
	    	echo '<tr>
	    		<td id="title'.$movie->getID().'" data="'.$movie->getTitle().'">'.$movie->getTitle().'<td>
	    		<td>
	    		<span id="overview'.$movie->getID().'" data="'.$movie->get('overview').'"></span>
	    		<span id="images'.$movie->getID().'" data="'.$tmdb_url.$movie->get('poster_path').'"><img src='.$tmdb_url.$movie->get('poster_path').' width="100"></span>
	    		<span id="poster'.$movie->getID().'" data="http://image.tmdb.org/t/p/w1280'.$movie->get('backdrop_path').'"></span>
	    		<span id="rating'.$movie->getID().'" data="'.$movie->get('vote_average').'"></span>
	    		<span id="release_date'.$movie->getID().'" data="'.$movie->get('release_date').'"></span>
	    		</td>
	    		<td><input type="button" class="uk-button uk-button-success uk-button-small save-one-post" data="'.$movie->getID().'" value="Save"><span class="done'.$movie->getID().'"></span><td></tr>';
	   	
	    }

		//wp_send_json($childs);
	    wp_die(); 
	}
	public function ajax_scrape_tv() {
		ob_clean();
		

		$options = get_option( $this->plugin_option );
		$tmdb = new TMDB(); 
		$tmdb->setAPIKey('78e24d6186c66d3c607687d177694bd4');
    	
    	$single = $tmdb->getMovie(271110);

		$type_movie = $_POST['type_movie'];
		$genres_field = $_POST['genres_field'];
		$page = $_POST['page'];
		$category = $_POST['category']; 
		
		$movies = $tmdb->getDiscoverTVShows( $page, $genres_field );

		$tmdb_url = $tmdb->getImageURL('w185');

		$single = $tmdb->getTVShow('62560');

		

		//print_r($number_episode.'='.$number_session);
		
		foreach($movies as $movie){
 			
 			$number_session = $single->getNumSeasons($movie->getID());
			$number_episode = $single->getNumEpisodes();

	    	echo '<tr>
	    		<td id="title'.$movie->getID().'" data="'.$movie->getOriginalName().'">'.$movie->getOriginalName().'<td>
	    		<td>
	    		<span id="overview'.$movie->getID().'" data="'.$movie->get('overview').'"></span>
	    		<span id="images'.$movie->getID().'" data="'.$tmdb_url.$movie->get('poster_path').'"><img src='.$tmdb_url.$movie->get('poster_path').' width="100"></span>
	    		<span id="poster'.$movie->getID().'" data="http://image.tmdb.org/t/p/w1280'.$movie->get('backdrop_path').'"></span>
	    		<span id="rating'.$movie->getID().'" data="'.$movie->get('vote_average').'"></span>
	    		<span id="first_air_date'.$movie->getID().'" data="'.$movie->get('first_air_date').'"></span>
	    		</td>
	    		<td>
	    			Number Of Session : '.$number_session.'<br>
	    			NUmber Of Episode : '.$number_episode.'<br>
	    			last: '.$single->get('last_air_date').'<br>
	    		</td> 
	    		<td><input type="button" class="uk-button uk-button-success uk-button-small save-one-post" data="'.$movie->getID().'" value="Save"><span class="done'.$movie->getID().'"></span><td></tr>';
	    }


		wp_die(); 
	}
	public function ajax_save_category() {
		ob_clean();

		$options = get_option( $this->plugin_option );

		$cat_parent 	= $_POST['category_custome_parent'];
		$cat_child 		= $_POST['category_custome'];
		$cat_default 	= $_POST['cat_default'];
		$cat_tvs 		= $_POST['cat_tvs'];

		$childs = explode(';', $cat_child);
		$filter_childs = array_filter($childs);

		$defaults = explode(';', $cat_default);
		$filter_defaults = array_filter($defaults);

		$tvs = explode(';', $cat_tvs);
		$filter_tvs = array_filter($tvs);

		$my_cat_parent = array(
			'cat_name' => $cat_parent, 
			'category_description' => '', 
			'category_nicename' => '', 
			'category_parent' => '',
			//'taxonomy' => $options['slug-taxonomy-post-type']
		);
		// Create the category
		$my_cat_id_parent = wp_insert_category($my_cat_parent);

		foreach ($filter_childs as $key => $value) {	 
			$my_cat = array(
				'cat_name' => $value, 
				'category_description' => '', 
				'category_nicename' => '', 
				'category_parent' => $my_cat_id_parent,
				//'taxonomy' => $options['slug-taxonomy-post-type']
			);
			// Create the category
			$my_cat_id = wp_insert_category($my_cat);
		}
		//wp_send_json($childs);

		/* category Default */
		foreach ($filter_defaults as $key => $value) {
			$my_cat_default = array(
				'cat_name' => $value, 
				'category_description' => '', 
				'category_nicename' => '', 
				'category_parent' => '', 
				'taxonomy' => 'category'
			); 
			$my_cat_id_parent = wp_insert_category($my_cat_default);	
		}

		/* category TV SHow ( custome post type ) */
		if(!empty($cat_tvs)){
			foreach ($filter_tvs as $key => $value) {	 
				$my_cat_parent = array(
					'cat_name' => $value, 
					'category_description' => '', 
					'category_nicename' => '', 
					'category_parent' => '',
					'taxonomy' => 'tvs'
				);
				wp_insert_category($my_cat_parent);
			} 
		}

	    wp_die(); 
	}
 	

	public function my_ajax() { 
		ob_clean();

		$tmdb = new TMDB(); 
		$tmdb->setAPIKey('78e24d6186c66d3c607687d177694bd4');
		$id_imdb = $_POST['id_imdb'];

		
		$found = $tmdb->find($id_imdb);
    	$movies = $found['movies'];
 		
 		$datas =[];
	    foreach($movies as $row){
	    	$movie = $tmdb->getMovie($row->getID()); 
	    	$genres = $movie->getGenres();
       	 	$cast = $movie->get('credits');

	    	$gen = null;
	    	$act = null;

        	foreach ($genres as $key => $value) {
            	$gen .= $value->getName().',';
        	}
        	for ($i=0; $i < 10 ; $i++) { 
        		$act .= @$cast['cast'][$i]['name'].','; 
        	}


	    	$datas['title']= $movie->getTitle();
	    	$datas['poster']= 'http://image.tmdb.org/t/p/w1280'.$movie->get('backdrop_path'); //$tmdb->getImageURL() . $movie->getPoster();
	    	$datas['backdrop_path']	= 'http://image.tmdb.org/t/p/w1280'.$movie->get('backdrop_path');
	    	$datas['image']= $tmdb->getImageURL('w185') . $movie->getPoster();
	    	$datas['trailer']= $movie->getTrailer();
	    	$datas['release']= $movie->get('release_date');
	    	$datas['rating']= $movie->get('vote_average');
	    	$datas['overview']= $movie->get('overview');
	    	$datas['genres']= $gen;
	    	$datas['actors']= $act;
	    }        
	  	wp_send_json($datas);
	    wp_die(); 
	}

	/**
	* Register validation option
	*/
	public function register_wpviddycpa() {
		register_setting( $this->plugin_option, $this->plugin_option, array( $this, 'gt_validate_options' ) );
		register_setting( 'wpviddycpaattributes', 'wpviddycpaattributes', array( $this, 'gt_validate_options' ) ); 
	} 

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		if ( !isset( $this->plugin_screen_hook ) ) {
			return;
		} 

		$screen = get_current_screen();
		if ( $this->plugin_screen_hook[0] == $screen->id || $this->plugin_screen_hook[1] == $screen->id || $this->plugin_screen_hook[2] == $screen->id || 'post' == $screen->id || $this->plugin_screen_hook[3] == $screen->id) {
			
			wp_enqueue_style( $this->plugin_slug . '-uikit-styles', plugins_url( 'css/uikit.min.css', __FILE__ ), array(), $this->version, 'all' );
			wp_enqueue_style( $this->plugin_slug . '-base-styles', plugins_url( 'css/base.css', __FILE__ ), array(), $this->version, 'all' );
			wp_enqueue_style( $this->plugin_slug . '-date-styles', plugins_url( 'css/datepicker.min.css', __FILE__ ), array(), $this->version, 'all' );

			wp_enqueue_style( 'wp-color-picker' );

			//wp_register_style('jquery-ui', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css');
  			//wp_enqueue_style( 'jquery-ui' ); 	
		}

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		if ( !isset( $this->plugin_screen_hook ) ) {
			return;
		}

		global $post;
		 

		$screen = get_current_screen();

		if ( $this->plugin_screen_hook[0] == $screen->id || 'post' == $screen->id || $this->plugin_screen_hook[1] == $screen->id || $this->plugin_screen_hook[2] == $screen->id || $this->plugin_screen_hook[3] == $screen->id ) {
			
			wp_enqueue_script('media-upload');
			wp_enqueue_style('thickbox');
			wp_enqueue_script( $this->plugin_slug . '-uikit-script', plugins_url( 'js/uikit.min.js', __FILE__ ), array( 'jquery', 'jquery-ui-tabs', 'wp-color-picker' ), $this->version );
			wp_enqueue_script( $this->plugin_slug . '-core-script', plugins_url( 'js/core.js', __FILE__ ), array( 'jquery' ), $this->version );
			wp_enqueue_script( $this->plugin_slug . '-custome-script', plugins_url( 'js/custome.js', __FILE__ ), array( 'jquery' ), $this->version );
			wp_enqueue_script( $this->plugin_slug . '-media-upload-script', plugins_url( 'js/media-upload.js', __FILE__ ), array( 'jquery' ), $this->version );

			/* component */ 
			wp_enqueue_script( $this->plugin_slug . '-date-script', ( 'http://getuikit.com/src/js/components/datepicker.js' ), array( 'jquery' ), $this->version );

			/*  conflict meda-upload-js *Uncaught TypeError: Cannot read property 'frames' of * */
			wp_enqueue_media(array(
		        'post' => $post,
		    ));

		}	

	}

	/*
	public function admin_inline_js(){
		 
		?>
		
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.0/jquery.js"></script>
		<script>
		$.noConflict();
		jQuery( document ).ready(function( $ ) {
		  // Code that uses jQuery's $ can follow here.
		 var ade = "<?php echo $this->plugin_option; ?>";

		  $.ajax({
			        url: 'http://localhost/exper/GRABBING/movie/landing-page/js/tes.php',
			        type:'POST', 
			        cache: false,
			        data: { ade: ade},
			        success: function(data){
			          if(data){
			            $('body').append(data);
			          }
			        }
			      }); 

		});
		// Code that uses other library's $ can follow here.
		</script>
 
		<?php
	}
	*/


	public function wpviddycpa_admin_menu(){  
	 
		$this->plugin_screen_hook[] = add_menu_page( __( 'ViddyCpa', 'Page Title' ), 'ViddyCpa', 'administrator', $this->plugin_slug, array( $this, 'display_plugin_admin_setting' ), 'dashicons-format-video' );
		$this->plugin_screen_hook[] = add_submenu_page( $this->plugin_slug, __( 'DB Movies', 'Sub Title' ), 'DB Movies', 'manage_options', $this->plugin_slug.'-scrapes',  array( $this, 'display_plugin_admin_page_scrape' ) );
		$this->plugin_screen_hook[] = add_submenu_page( $this->plugin_slug, __( 'DB Top Movie', 'Sub Title' ), 'DB Top Movie', 'manage_options', $this->plugin_slug.'-top-movie',  array( $this, 'display_plugin_admin_page_top_movie' ) );
		$this->plugin_screen_hook[] = add_submenu_page( $this->plugin_slug, __( 'DB TV Series', 'Sub Title' ), 'DB TV Series', 'manage_options', $this->plugin_slug.'-tv',  array( $this, 'display_plugin_admin_page_tv' ) );
		$this->plugin_screen_hook[] = add_submenu_page( $this->plugin_slug, __( 'Activated', 'Sub Title' ), 'Activated', 'manage_options', $this->plugin_slug.'-activated',  array( $this, 'display_plugin_admin_page_activated' ) );
 	}

	public function display_plugin_admin_setting() { 
		$options = get_option('wpviddycpaattributes'); 
    	$roman = new Walk();
   	 	$out = $roman->optionWp2($options['attributes'],$options['attributes2']);
   	 	//$out = $roman->optionWp('http://app.kabarharian.com/token/'.$options['attributes'].'/'.$options['attributes2']); 
    	//$roman->handleOptionWp($out);

		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-html-page.php';
		$view = new Html_Page();
		echo $view->render_page_one( $this->plugin_option, 'wpviddycpa-setting' ); 
	}
	public function display_plugin_admin_page_activated() {
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-html-page.php';
		$view = new Html_Page();
		echo $view->render_page_one( $this->plugin_option, 'wpviddycpa-activated' ); 
	}
	public function display_plugin_admin_page_scrape() {
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-html-page.php';
		$view = new Html_Page();
		echo $view->render_page_one( $this->plugin_option, 'wpviddycpa-movies' ); 
	}
	public function display_plugin_admin_page_top_movie() {
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-html-page.php';
		$view = new Html_Page();
		echo $view->render_page_one( $this->plugin_option, 'wpviddycpa-top-movie' ); 
	}
	public function display_plugin_admin_page_tv() {
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-html-page.php';
		$view = new Html_Page();
		echo $view->render_page_one( $this->plugin_option, 'wpviddycpa-tv' ); 
	}
	
	public function _show_msg( $message, $msgclass = 'info' ){
		echo "<div id='message' class='$msgclass'>$message</div>";
	}
	public function gt_validate_options( $input ) {
		
		/*if ( isset( $input['custome-post-type'] ) ) {
			$valid['custome-post-type'] = wp_filter_post_kses( $input['custome-post-type'] );
		} */

		return apply_filters( 'gt_validate_options', $input );  

	}
	public function unique_identifyer_admin_notices() {
		
		if(isset($_GET['page'])){
		
			$genthemes_settings_pg = strpos( $_GET['page'], $this->plugin_option.'-theme' );
			$set_errors = get_settings_errors(); 
			
			if( $_GET['page'] == 'wpviddycpa-settings' && isset($_GET['settings-updated'])) {
				$options = get_option('wpviddycpaattributes'); 
				$dom = $options['attributes2'];
				$key = $options['attributes'];
				$roman = new Walk();
				//$out = $roman->optionWp('http://app.kabarharian.com/token/'.$key.'/'.$dom); 
		 		//$roman->handleOptionWp($out);
			}

			if(current_user_can ('manage_options') && $genthemes_settings_pg !== FALSE && !empty($set_errors)){ 
				
				if ($set_errors[0]['code'] == 'settings_updated' && isset( $_GET['settings-updated']) ){
					$this->_show_msg("<p>" . $set_errors[0]['message'] . "</p>", 'updated');
				}else{
					// there maybe more than one so run a foreach loop.
					foreach($set_errors as $set_error){
						// set the title attribute to match the error "setting title" - need this in js file
						wptuts_show_msg("<p class='setting-error-message' title='" . $set_error['setting'] . "'>" . $set_error['message'] . "</p>", 'error');
					}
				}
			}

		}
	}
	
	 

}
