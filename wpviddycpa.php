<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://www.linkedin.com/in/berbagimadani
 * @since             1.0.0
 * @package           wpviddycpa
 *
 * @wordpress-plugin
 * Plugin Name:       Landing Page Guardian CPA movies and videos
 * Plugin URI:        http://jv.kabarharian.com/landingpage/
 * Description:       Scripts landing page cpa movies, videos, and download archive
 * Version:           1.0.0
 * Author:            Ade iskandar
 * Author URI:        http://kabarharian.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       wpviddycpa
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-ApVideo_Curate-activator.php
 */
function activate_wpviddycpa() {
 
	$plugin = new Wpviddycpa(); 
 		
 	$deprecated = null;
  	$autoload = 'yes';

	if ( get_option( $plugin->get_plugin_slug() ) !== false ) {
	    update_option( $plugin->get_plugin_slug(), '' ); 
	}
	else{
		add_option( $plugin->get_plugin_slug(), '', $deprecated, $autoload );  
	}

}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-ApVideo_Curate-deactivator.php
 */
function deactivate_wpviddycpa() {
	
	$plugin = new Wpviddycpa();
	update_option( $plugin->get_plugin_slug(), 'closed' ); 

}

register_activation_hook( __FILE__, 'activate_wpviddycpa' );
register_deactivation_hook( __FILE__, 'deactivate_wpviddycpa' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-wpviddycpa.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_wpviddycpa() {

	$plugin = new Wpviddycpa();
	$plugin->run();

}
run_wpviddycpa();
