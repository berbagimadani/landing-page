<style type="text/css">
  /*!
 * Yamm!3 - Yet another megamenu for Bootstrap 3
 * http://geedmo.github.com/yamm3
 * 
 * @geedmo - Licensed under the MIT license
 */
.yamm .nav,
.yamm .collapse,
.yamm .dropup,
.yamm .dropdown {
  position: static;
}
.yamm .container {
  position: relative;
}
.yamm .dropdown-menu {
  left: auto;
}
.yamm .yamm-content {
  padding: 20px 30px;
}
.yamm .dropdown.yamm-fw .dropdown-menu {
  /*left: 0;
  right: 0;*/
}
.yamm-content li a{
  font-size: 14px;
  text-transform: uppercase;
  line-height: 32px;
  color: #000; 
}
 
</style>
<div class="collapse navbar-collapse js-navbar-collapse navbar-collapse-no-right"> 
  <?php
    wp_nav_menu( array(
        'menu'              => 'lpguardian-menu',
        'theme_location'    => 'lpguardian-menu',
        'depth'             => 2,
        'container'         => 'div',
        'container_class'   => 'collapse navbar-collapse',
        'container_id'      => 'bs-example-navbar-collapse-1',
        'menu_class'        => 'nav navbar-nav yamm',
        'fallback_cb'       => 'Yamm_Fw_Nav_Walker_menu_fallback',
        'walker'            => new Yamm_Fw_Nav_Walker())
    );
?>

<div class="col-md-4 pull-right box-search" style="text-align:right">
<form role="search" method="get" id="searchform" class="searchform" action="<?php echo esc_url( home_url(  SLUG_CUSTOME_POST.'/' ) ); ?>">
    <div>
        <label class="screen-reader-text" for="s"><?php _x( 'Search for:', 'label' ); ?></label>
        <input type="text" value="<?php echo get_search_query(); ?>" name="s" id="s" class="box-search-input" placeholder="Enter keyword"/>
        <input type="submit" id="searchsubmit" value="<?php echo esc_attr_x( 'Search', 'submit button' ); ?>" class="btn" />
    </div>
</form>
</div>

</div>
