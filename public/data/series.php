<?php

/**
 * Video 1
*/
$data['series']['ID__IMDB']				= '';

/* play */
$data['series']['player']				= 'youtube';
$data['series']['url_video']			= 'NZSxu-UKLV8';
$data['series']['url_image']			= 'http://img.123movies.to/2016/06/17/cover/32fa9e38de65f1ec2c29b69000b86325-orange-is-the-new-black-season-4-1466189608.jpg';

$data['series']['type_cta']				= 'openlink'; 
$data['series']['url_cta']				= 'http://yournetwork';

$data['series']['locker']				= 1;
$data['series']['url_referral']			= 'http://html.iwthemes.com/tv/';
$data['series']['duration_show_offer']	= 0;

/* download */
$data['series']['download:type_cta']	= 'openlink'; 
$data['series']['download:url_cta']		= 'http://yournetwork';
$data['series']['download:locker']		= 1;
$data['series']['download:url_referral']= 'http://html.iwthemes.com/tv/';

/* optional */
$data['series']['title'] 			= 'Orange is the New Black - Season 4';
$data['series']['description']		= 'Who will fight, and who will fall? Season 4 opens with a major security breach and a lot of new inmates, therefore Caputo has to call in the big guns. Things get a little too real for Crazy Eyes and Lolly.';
$data['series']['image']			= 'http://img.123movies.to/2016/06/17/poster/062da9031c92a403efbcde9e1a2772bc-orange-is-the-new-black-season-4-1466189607.jpg';
$data['series']['rating']			= '9.0';
$data['series']['actors']			= 'Taylor Schilling, Danielle Brooks, Taryn Manning';
$data['series']['genres']			= 'Comedy, Crime, Drama';
$data['series']['duration']			= '59 min';
$data['series']['release']			= '2016'; 
$data['series']['view_watch']		= '1000 view';

 




echo json_encode($data);
?>