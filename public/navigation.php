<div class="container">
  <nav class="navbar yamm navbar-default">
    <div class="navbar-header">
      <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".js-navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <!-- logo  --> 
        <?php if(empty(getOption('logo'))) : ?>
            <a class="navbar-brand" href="<?php echo site_url().'/'.$slug_custome_post; ?>"><?php echo bloginfo(); ?></a>
        <?php endif; ?>
        <?php if(!empty(getOption('logo'))) :?>
            <a href="<?php echo site_url().'/'.$slug_custome_post; ?>"><img src="<?php  echo getOption('logo'); ?>" width="220"></a>
        <?php endif; ?>
    </div>

    
    <?php require plugin_dir_path( __FILE__ ) . 'menu.php'; ?>
    <!-- /.nav-collapse -->
  </nav>
</div>