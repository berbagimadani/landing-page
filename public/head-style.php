    
    <!--
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo $options['apple-touch-icon'];?>">
    <link rel="icon" type="image/png" href="<?php echo $options['favicon-32x32'];?>" sizes="32x32">
    <link rel="icon" type="image/png" href="<?php echo $options['favicon-16x16'];?>" sizes="16x16"> -->
    
    <link rel="shortcut icon" href="<?php echo $options['favicon'];?>">

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo plugins_url( 'css/bootstrap.min.css', __FILE__ ); ?>" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo plugins_url( 'css/landing-page.css', __FILE__ ); ?>" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo plugins_url( 'font-awesome/css/font-awesome.min.css', __FILE__ ) ?>" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://cdn.plyr.io/1.6.16/plyr.css">
    <link rel="stylesheet" href="<?php echo plugins_url( 'css/popup.css', __FILE__ ) ?>">
    <link rel="stylesheet" href="<?php echo plugins_url( 'social-share/jssocials.css', __FILE__ ) ?>">
    <link rel="stylesheet" href="<?php echo plugins_url( 'social-share/jssocials-theme-flat.css', __FILE__ ) ?>">
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <style>
        /* Some basic style resets to make the page look slightly nicer */
        h1, h2 { 
            margin: 10px 0;
        }

        p, a { 
            font-size: 13px;
        }

        /* Gallery */
        [href="#next"] {
            float: right;
        }

        [href="#prev"] {
            float: left;
        }

        [href="#next"], [href="#prev"] {
            padding-top: 10px;
        }

        div.popup {
            overflow: hidden;
        }
        <?php $options = get_option('wpviddycpa-option'); ?>
        .intro-header {
            padding-top: 20px; /* If you're making other pages, make sure there is 50px of padding to make sure the navbar doesn't overlap content! */
            padding-bottom: 50px;
            text-align: center;
            background: url("<?php echo $options['background'];?>") no-repeat center center;
            background-size: cover;
        }
        .banner {
            padding: 100px 0;
            color: #f8f8f8;
            background: url("<?php echo $options['banner'];?>") no-repeat center center;
            background-size: cover;
        }
        .main-background2{
            background-color: rgba(255,255,255,0.8); 
        }
    </style> 

<?php 
$slug_custome_post =  !empty($options['slug-custome-post-type']) ? $options['slug-custome-post-type'] : '';
$slug_taxonomy_post =  !empty($options['slug-taxonomy-post-type']) ? $options['slug-taxonomy-post-type'] : ''; 

//define('SLUG_CUSTOME_POST', $slug_custome_post );
//define('SLUG_TAXONOMY_POST', $slug_taxonomy_post );

function getOption($var){
    $options = get_option('wpviddycpa-option');
    if(!empty($var))
        return @$options[$var];
}
?>

<?php echo getOption('ads-scripts'); ?>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.7&appId=430707847037600";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>