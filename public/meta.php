<?php
function getOption2($var){
    $options = get_option('wpviddycpa-option');
    if(!empty($var))
        return @$options[$var];
}
?>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="description" content="<?php echo getOption2('meta-description'); ?>"/>
    <meta name="keywords" content="<?php echo getOption2('meta-keyword'); ?>"/>

    <title><?php echo bloginfo(); ?></title>