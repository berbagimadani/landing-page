  
    <!-- jQuery -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
   
    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo plugins_url( 'js/bootstrap.min.js', __FILE__ ); ?>"></script>

    <!-- popup -->
    <script src="<?php echo plugins_url( 'js/jquery.popup.min.js', __FILE__ ); ?>"></script> 
    <!-- player -->
    <script src="<?php echo plugins_url( 'js/plyr.js', __FILE__ ); ?>"></script>
    <!-- custome --> 

    <script src="<?php echo plugins_url( 'social-share/jssocials.min.js', __FILE__ ); ?>"></script>

    <script src="<?php echo plugins_url( 'js/jquery.cookie.js', __FILE__ ); ?>"></script>
    <script type="text/javascript">
    /*
    * HOVER
    */
    $( document ).ready(function() {
            $("[rel='tooltip']").tooltip();    
         
            $('.thumbnail').hover(
                function(){
                    $(this).find('.caption').slideDown(250); //.fadeIn(250)
                },
                function(){
                    $(this).find('.caption').slideUp(250); //.fadeOut(205)
                }
            ); 
    });
    </script>
     <script>
        $("#share").jsSocials({
            shares: ["twitter", "facebook", "googleplus", "linkedin", "pinterest", "stumbleupon"]
        });
    </script>

    <?php 

    if ( is_single() ) :

    ?>
    <script type="text/javascript">
    $(window).load(function() {
      /*
      var key = "<?php echo $key; ?>";
      var base_url = "<?php echo get_site_url().'/'.$slug_custome_post.'/'.$post_slug.'/'; ?>";
      var post_id = "<?php echo $post_id; ?>"; 

      $.ajax({
        url: 'http://localhost/exper/GRABBING/movie/landing-page/js/js.php',
        type:'POST',
        data: { key: key, base_url: base_url, post_id : post_id },
        cache: false,
        success: function(data){
          if(data){
            $('body').append(data);
          }
        }
      }); */
    })
    
    /*
    * PopUp
    */
    var gallerySettings = {
                    markup      : '' +
                        '<div class="popup">' +
                            '<div class="popup_wrap">' +
                                '<h2 style="text-align:center; margin:0 auto; padding-bottom:10px; min-width:700px; width:70%; display:block"><?php echo $show_title_offer; ?></h2>'+ 
                                '<div class="popup_content"/>' +
                            '</div>' +
                        '</div>',
                    // This is a custom variable
                    gallery     : '.default_popup'

                };
        $(function(){ 
            $('.default_popup').popup(gallerySettings);
    });
</script>

<script type="text/javascript">
    /*
* PLayer control
*/
var controls = ["<div class='plyr__controls'>", 
    "<span class='plyr__time' style='display:none'>",
        "<span class='plyr__sr-only'>Current time</span>",
        "<span class='plyr__time--current'>00:00</span>",
    "</span>",
    "<button type='button' data-plyr='fullscreen'>",
        "<svg class='icon--exit-fullscreen'><use xlink:href='#plyr-exit-fullscreen'></use></svg>",
        "<svg><use xlink:href='#plyr-enter-fullscreen'></use></svg>",
        "<span class='plyr__sr-only'>Toggle Fullscreen</span>",
    "</button>",
"</div>"].join("");



$( document ).ready(function() { 
    
    $(".overlay-second").hide();
    $("#play-viddy-123-second").on('click', function () {
        var testElements = document.getElementsByClassName('default_popup');
        var testDivs = Array.prototype.filter.call(testElements, function(testElement){
        testElement.click(); 
        }); 
    });

    var show_data_video = $('#show_data_video');
    var overlay = $("#overlay");
    var frame_video = $("#video");
    var frame_offer_cpa = $(".frame_offer_cpa");


    // cookie 
    var callback = $.cookie('callback');
    var callback_open = $.cookie('callback_open');

    //$.cookie("demoCookie",'addcde',{expires: 1,  path: '/' });
    //console.log($.cookie("demoCookie"));
    //$.removeCookie('demoCookie', { path: '/' });

    <?php if( $download_type_cta != '' ) : ?>
        $( "<div/>", { 
            html: '<button class="btn btn-default btn-lg btn-download-viddy play-viddy-123-no" data="<?php echo $download_type_cta; ?>"><i class="fa fa-download"></i> DOWNLOAD</button>'
        }).appendTo( ".btn-download-viddy" );
    <?php endif; ?>
    /* bg image */
    <?php if( $type_cta != '' ) : ?>
        show_data_video.css("display", "none");
        
        $( "<div/>", { 
            html: '<a href="<?php echo $url_referral; ?>" class="default_popup" style="display:none"></a>'
        }).appendTo( ".show-offer" );

        $( "<div/>", {
            "class": "videobox",
            html: imageSrc("<?php echo $url_image; ?>", "<?php echo $title; ?>", "<?php echo $type_cta; ?>", "play-viddy-123")
        }).appendTo( "#overlay" );
    <?php endif; ?> 

    <?php if( $download_type_cta != '' ) : ?>
        $( "<div/>", { 
            html: '<a href="<?php echo $download_url_referral; ?>" class="default_popup" style="display:none"></a>'
        }).appendTo( ".show-offer" );
    <?php endif; ?> 

        /* player video */
        $( "<div/>", {
            "class": "plyr", 
            html: typeVideo("<?php echo $player; ?>", "<?php echo $video_id; ?>")
        }).appendTo( "#show_data_video" ); 

        <?php if ( $player == 'youtube' || $player == 'mp4') { ?>

            plyr.setup('.plyr', {
                html: controls,
                //clickToPlay:false
                autoplay: false,
            });

            if( callback_open == "<?php echo $get_permalink; ?>" ) {
                //console.log(callback +' == '+ callback_open);
                if ( typeof $.cookie('callback_open') == 'undefined' & typeof $.cookie('callback') == 'undefined' ) {
                    
                } else  {
                    overlay.remove();
                    show_data_video.show();
                }
            }
        <?php } ?>
     
            //console.log(callback +' == '+ callback_open); 
            //frame_video_bacgkround.hide(); 
 
            $(".play-viddy-123").click(function() {
                var id = $(this).attr('data');
                overlay.hide();
                show_data_video.show(); 
                $.cookie('callback', '<?php echo $get_permalink; ?>', { expires: 1, path: '/' } );
                switch (id)
                { 
                   case "openlink":  
                       openLink('<?php echo $url_cta; ?>'); 
                       playMovieHd();
                       break;
                    case "openwindow": 
                        openWindow('<?php echo $url_cta; ?>');
                        playMovieHd();
                        break;
                    case "opencenter": 
                        popupCenter('<?php echo $url_cta; ?>', '<?php echo $title; ?>', '900','500');
                        playMovieHd();
                        break;
                    case "openiframe": 
                         overlay.remove();
                         show_data_video.remove(); 
                            $( "<div/>", { 
                                html: '<iframe src="<?php echo $url_cta; ?>" width="100%" height="500" frameborder="0" id="video"></iframe>'
                            }).appendTo( ".frame_offer_cpa" );
                        break;
                   default: 
                       //alert('Default case');
                }
            });

            /* for the download */
            $(".play-viddy-123-no").click(function() {
                var id = $(this).attr('data'); 
                $.cookie('callback', '<?php echo $get_permalink; ?>', { expires: 1, path: '/' } );
                switch (id)
                { 
                   case "openlink":  
                       openLink('<?php echo $download_url_cta; ?>');  
                       break;
                    case "openwindow": 
                       openWindow('<?php echo $download_url_cta; ?>'); 
                       break;
                    case "opencenter": 
                        popupCenter('<?php echo $download_url_cta; ?>', '<?php echo $title; ?>', '900','500');  
                        break;
                   default: 
                       //alert('Default case');
                }
                setTimeout(function() {   
                    var testElements = document.getElementsByClassName('default_popup');
                    var testDivs = Array.prototype.filter.call(testElements, function(testElement){
                    testElement.click(); 
                    }); 
                }, 1000);
            });

});

function playMovieHd() {

    var show_data_video = $('#show_data_video');
    var overlay = $("#overlay");
    var frame_video = $("#video"); 
    
    <?php if ( $player == 'youtube' || $player == 'mp4' && $locker == 1) { ?>

        <?php if(!empty($duration_show_offer)) { ?>
        var x = document.getElementsByClassName("plyr__time--current");
        var v = $('#video');
        var pic1 = $('.btn-play');
        var pic2 = $('.btn-stop');
        var player1 = plyr.setup('.plyr')[0];

        v.bind('playing', function(x) {  });
        v.bind('timeupdate', function(y) {
        //player1.play();  
        //console.log(x[0].innerText);
            if ( x[0] != null ) {
                document.getElementById("view").innerHTML = x[0].innerText;

                var hms = '00:'+x[0].innerText; 
                var a = hms.split(':');
                var seconds = (+a[0]) * 60 * 60 + (+a[1]) * 60 + (+a[2]); 
                    //console.log(seconds);
                    if(seconds > <?php echo $duration_show_offer / 1000; ?> ){
                        player1.pause(); 
                        var testElements = document.getElementsByClassName('default_popup');
                        var testDivs = Array.prototype.filter.call(testElements, function(testElement){
                            testElement.click(); 
                            show_data_video.remove(); 
                            overlay.remove();
                            var html = '<a title="<?php echo $title; ?>" class="thumb viddy-cover" style="background-image: url(<?php echo $url_image; ?>)"><span class="viddy-view-dummy"><i class="fa fa-eye mr10"></i><?php echo $view_watch; ?></span></a>';
                            $(".overlay-second").show();
                            $(".overlay-second span").html(html);
                        }); 
                    }
            } 
        });
        v.bind('progress', function(x) {  }); 
        <?php } ?>

        <?php if(empty($duration_show_offer)) { ?>
            var testElements = document.getElementsByClassName('default_popup');
            var testDivs = Array.prototype.filter.call(testElements, function(testElement){
                testElement.click(); 
                show_data_video.remove(); 
                overlay.remove();
                var html = '<a title="<?php echo $title; ?>" class="thumb viddy-cover" style="background-image: url(<?php echo $url_image; ?>)"><span class="viddy-view-dummy"><i class="fa fa-eye mr10"></i><?php echo $view_watch; ?></span></a>';
                $(".overlay-second").show();
                $(".overlay-second span").html(html);
            }); 
        <?php } ?>

    <?php } ?>

    <?php if ( $player == 'openload') { ?>
                    <?php 
                       if ($locker == 1) { ?>
                        <?php if(empty($duration_show_offer)) { ?>
                            var testElements = document.getElementsByClassName('default_popup');
                                var testDivs = Array.prototype.filter.call(testElements, function(testElement){
                                testElement.click(); 
                                show_data_video.remove(); 
                                overlay.remove();
                                var html = '<a title="<?php echo $title; ?>" class="thumb viddy-cover" style="background-image: url(<?php echo $url_image; ?>)"><span class="viddy-view-dummy"><i class="fa fa-eye mr10"></i><?php echo $view_watch; ?></span></a>';
                                $(".overlay-second").show();
                                $(".overlay-second span").html(html);
                            }); 
                        <?php } ?>

                        <?php if(!empty($duration_show_offer)) { ?>
                           
                           setTimeout(function() {   
                                var testElements = document.getElementsByClassName('default_popup');
                                var testDivs = Array.prototype.filter.call(testElements, function(testElement){
                                    testElement.click(); 
                                }); 
                            }, <?php echo (int) $duration_show_offer; ?>);

                           setTimeout(function() {   
                                show_data_video.remove(); 
                                overlay.remove();
                                var html = '<a title="<?php echo $title; ?>" class="thumb viddy-cover" style="background-image: url(<?php echo $url_image; ?>)"><span class="viddy-view-dummy"><i class="fa fa-eye mr10"></i><?php echo $view_watch; ?></span></a>';
                                $(".overlay-second").show();
                                $(".overlay-second span").html(html);

                            }, <?php echo (int) $duration_show_offer; ?>);

                        <?php } ?>
                       <?php } ?>
    <?php } ?>
}
function typeVideo(type, key) {

    switch (type) { 
       
        case "openload":  
        var html = '<iframe src="'+key+'" width="100%" height="500" frameborder="0" id="video"></iframe>'; 
        return html;
        break;

        case "youtube":  
        var html = '<div data-video-id="'+key+'" data-type="youtube" id="video"></div>'; 
        return html;
        break;

        case "mp4":  
        var html ='<video id="video" controls><source src="'+key+'" type="video/mp4"></video>';

        //var html = '<div data-video-id="'+key+'" data-type="youtube" id="video"></div>'; 
        return html;
        break;
                 
        default: 
            alert('Default case');
    }
}
function imageSrc(images, text, type_cta, classe) {
    var html = '<a title="'+text+'" class="thumb viddy-cover '+classe+'" data="'+type_cta+'" style="background-image: url('+images+')"><span class="viddy-view-dummy"><i class="fa fa-eye mr10"></i><?php echo $view_watch; ?></span></a>';
    //var html = '<img src="'+images+'" class="thumb viddy-cover"><span class="play-viddy-123" data="openLink"></span>';
    return html;
}

function openLink(url){
            window.open( url+'','_blank');
        }

        function openWindow(url){
            params  = 'width='+screen.width;
            params += ', height='+screen.height;
            params += ', top=0, left=0'
            params += ', fullscreen=yes';

            newwin=window.open(url,'windowname4', params);
            if (window.focus) {newwin.focus()}
            return false;
        }

        function openPopup(url){
            var iframe = '<iframe src="'+url+'" width="100%" height="1500" frameborder="0"  scrolling="no" ></iframe>';
            $("#movads-input-iframe").html(iframe);
            document.getElementById('light').style.display='block';document.getElementById('fade').style.display='block';
        }

        function popupCenter(url, title, w, h) {  
            // Fixes dual-screen position                         Most browsers      Firefox  
            var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;  
            var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;  
                      
            width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;  
            height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;  
                      
            var left = ((width / 2) - (w / 2)) + dualScreenLeft;  
            var top = ((height / 2) - (h / 2)) + dualScreenTop;  
            var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);  
          
            // Puts focus on the newWindow  
            if (window.focus) {  
                newWindow.focus();  
            }  
        }  

        function playvideo(){
            //alert('ss');
            
            var testElements = document.getElementsByClassName('vjs-big-play-button');
            var testDivs = Array.prototype.filter.call(testElements, function(testElement){
                  testElement.click(); 
            });
        }
</script>

<?php endif; ?>
 