<!DOCTYPE html>
<html lang="en">

<head>

    <?php require plugin_dir_path( __FILE__ ) . 'meta.php'; ?>
    <?php require plugin_dir_path( __FILE__ ) . 'head-style.php'; ?>

</head>

<body>

    <!-- Navigation -->
    <?php require plugin_dir_path( __FILE__ ) . 'navigation.php'; ?>

	<?php global $wp_query; ?>
	<?php 
	
	if( !empty($wp_query->query_vars['taxonomy'])){	
		$slug_tax = $wp_query->query_vars[SLUG_TAXONOMY_POST];
	} 
	?>
    <!-- Header -->
    <a name="about"></a>
    <div class="intro-header">
        <div class="container">
        
            <div class="row main-background2">
                <div class="col-lg-12 canvas-viddy-desc">
                    <div class="intro-message">
                    	<h2> 
                    	<?php
                    		$termby = get_term_by('slug', $slug_tax, SLUG_TAXONOMY_POST); 
						   	echo $termby->name; 
                    	?>
                    	</h2>
                    </div>
                </div>
            </div>

        </div>
        <!-- /.container -->

    </div>
    <!-- /.intro-header -->

    <!-- Page Content -->

	<a  name="services"></a>
    <div class="content-section-a">

        <div class="container">
            <div class="row no-getter"> 
                <div class="col-md-12">
                    <h3><?php echo $termby->name;?></h3>  
                </div>
                <?php 

                   //$currentCategory = single_cat_title("", false);
                    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; 
 
                    $args = array(
                        'post_type' => SLUG_CUSTOME_POST,
                         SLUG_TAXONOMY_POST => $slug_tax,
                        'paged'=> $paged,
                        'order' => 'DESC',
                        'posts_per_page' => '20'
                    );  
                    $the_query = new WP_Query( $args ); 
            if($the_query->have_posts()) :     

                    while ($the_query->have_posts()):
                        $the_query->the_post();   
                        $featured_image_array = wp_get_attachment_image_src( get_post_thumbnail_id(), 'single-post-thumbnail' );
                        $featured_image = $featured_image_array[0];
                   ?>  
                <div class="col-md-2 col-xs-4 box-thumb-images"> 
                    <div class="thumbnail no-border">
                        <a href="<?php echo get_permalink(); ?>">
                        <div class="caption">
                            <h5><?php echo get_post_meta(get_the_ID(),'wpviddycpa-meta-title',TRUE) ?> </h5>  
                            <p style="font-size:11px">
                             <?php 
                                $text = get_post_meta(get_the_ID(),'wpviddycpa-meta-description',TRUE);
                                echo wp_trim_words( $text, $num_words = 18, $more = null );
                              ?> 
                            </p>
                            <p><span class="label label-danger" rel="tooltip" title="Release year"><?php echo get_post_meta(get_the_ID(),'wpviddycpa-meta-release',TRUE) ?> </span>
                            </p>
                             <p>
                            <span class="label label-viddy-orange" rel="tooltip" title="IMDB Rate">
                            IMDb:
                            <?php echo get_post_meta(get_the_ID(),'wpviddycpa-meta-rating',TRUE) ?>
                            </span>
                            </p>
                        </div>
                        <img class="img-responsive" src="<?php echo get_post_meta(get_the_ID(),'wpviddycpa-meta-image',TRUE) ?>">
                        </a>
                    </div>
                    <a href="<?php echo get_permalink(); ?>" class="text-center">
                        <h5><?php echo get_post_meta(get_the_ID(),'wpviddycpa-meta-title',TRUE) ?> </h5> 
                    </a>
                </div>
                <?php endwhile; ?> <?php wp_reset_query(); ?> 
                
                <div class="col-md-12">
                <?php if ($the_query->max_num_pages > 1) { // check if the max number of pages is greater than 1  ?>
                   <?php
                    $big = 999999999; // need an unlikely integer
                    echo paginate_links( array(
                        'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                        'format' => '?paged=%#%',
                        'current' => max( 1, get_query_var('paged') ),
                        'total' => $the_query->max_num_pages
                    ) );
                   ?>
                <?php } ?>

                <?php else: ?>
                  <article>
                    <h2>Sorry...</h2>
                    <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
                  </article>
                </div>
            <?php endif; ?>

            <div class="wpviddy-ads1">
                 <?php echo getOption('ads-banner-single'); ?>
            </div>
                
            </div>

        </div>
        <!-- /.container -->

    </div>
    <!-- /.content-section-a -->
 
    <hr> 
	<!--<a  name="contact"></a>-->
    <div class="banner">
        <div class="container">

            <div class="row">
                <div class="col-lg-6">
                    <h2>Connect to social media:</h2>
                </div>
                <div class="col-lg-6">
                    <div id="share"></div>
                </div>
            </div>
        </div> 
    </div>

    <!-- Footer -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                     
                    <?php if ( has_nav_menu( 'primary' ) ) : ?> 
                            <?php
                                // Primary navigation menu.
                                wp_nav_menu( array(
                                    'menu_class'     => 'list-inline',
                                    'theme_location' => 'primary',
                                ) );
                            ?> 
                    <?php endif; ?> 
                    <div id="show-data"></div>
                    <p class="copyright text-muted small">
                         <?php echo getOption('copyright'); ?>
                    </p>
                </div>
            </div>
        </div>
    </footer>

    <?php require plugin_dir_path( __FILE__ ) . 'footer.php'; ?>
</body>
</html>
