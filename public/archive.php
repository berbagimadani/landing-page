<!DOCTYPE html>
<html lang="en">

<head>

    <?php require plugin_dir_path( __FILE__ ) . 'meta.php'; ?>
    <?php require plugin_dir_path( __FILE__ ) . 'head-style.php'; ?>

</head>

<body>

<!-- Navigation -->
<?php require plugin_dir_path( __FILE__ ) . 'navigation.php'; ?>

    <!-- Header -->
    <a name="about"></a>
    <div class="intro-header">
        <div class="container">
            
            <div class="row main-background">
                <div class="col-lg-12 canvas-viddy-desc">
                    <?php 
                     

                    $args_one = array( 'post_type' => SLUG_CUSTOME_POST,  'order' => 'DESC', 'posts_per_page' =>  1);  
                    $the_query_one = new WP_Query( $args_one );   
                        while ($the_query_one->have_posts()):
                        $the_query_one->the_post();

                    ?>  
                    <?php $url_images = get_post_meta(get_the_ID(),'wpviddycpa-meta-url_image',TRUE); ?>
                    <div class="intro-message">
                       <a href="<?php echo the_permalink(); ?>">
                       <div id="overlay"><div class="videobox"><span class="thumb viddy-cover" title="<?php echo the_title(); ?>" style="background-image: url(<?php echo $url_images; ?>)"><span class="viddy-view-dummy"><i class="fa fa-eye mr10"></i><?php echo get_post_meta(get_the_ID(),'wpviddycpa-meta-view_watch',TRUE);?></span></span></div></div>
                        </a>
                    </div>
                    <?php endwhile; ?>
                </div>
            </div>

        </div>
        <!-- /.container -->

    </div>
    <!-- /.intro-header -->

    <!-- Page Content -->

	<a  name="services"></a>
    <div class="content-section-a">

        <div class="container">
            <div class="row no-getter"> 
                <div class="col-md-12">
                    <h3>Latest Movies</h3>  
                </div>
                <?php 

                   //$currentCategory = single_cat_title("", false);
                    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                    $args = array(
                        'post_type' => SLUG_CUSTOME_POST,
                        //'gallery_category' => 'video-gallery',
                        'paged'=> $paged,
                        'order' => 'DESC',
                        'posts_per_page' => '20'
                    );  
                    $the_query = new WP_Query( $args ); 
            if($the_query->have_posts()) :     

                    while ($the_query->have_posts()):
                        $the_query->the_post();   
                        $featured_image_array = wp_get_attachment_image_src( get_post_thumbnail_id(), 'single-post-thumbnail' );
                        $featured_image = $featured_image_array[0];
                   ?>  
                <div class="col-md-2 col-xs-4 box-thumb-images"> 
                    <div class="thumbnail no-border">
                        <a href="<?php echo get_permalink(); ?>">
                        <div class="caption">
                            <h5><?php echo get_post_meta(get_the_ID(),'wpviddycpa-meta-title',TRUE) ?> </h5>  
                            <p style="font-size:11px">
                             <?php 
                                $text = get_post_meta(get_the_ID(),'wpviddycpa-meta-description',TRUE);
                                echo wp_trim_words( $text, $num_words = 18, $more = null );
                              ?> 
                            </p>
                            <p>
                            <span class="label label-danger" rel="tooltip" title="Release year"><?php echo get_post_meta(get_the_ID(),'wpviddycpa-meta-release',TRUE) ?> </span>
                            </p>

                             <p>
                            <span class="label label-viddy-orange" rel="tooltip" title="IMDB Rate">
                            IMDb:
                            <?php echo get_post_meta(get_the_ID(),'wpviddycpa-meta-rating',TRUE) ?>
                            </span>
                            </p>

                        </div>
                        <img class="img-responsive" src="<?php echo get_post_meta(get_the_ID(),'wpviddycpa-meta-image',TRUE) ?>">
                        </a>
                    </div>
                    <a href="<?php echo get_permalink(); ?>" class="text-center">
                        <h5><?php echo get_post_meta(get_the_ID(),'wpviddycpa-meta-title',TRUE) ?> </h5> 
                    </a>
                </div>
                <?php endwhile; wp_reset_postdata();?>
                
                <div class="col-md-12">
                <?php if ($the_query->max_num_pages > 1) { // check if the max number of pages is greater than 1  ?>
                   <?php
                    $big = 999999999; // need an unlikely integer
                    echo paginate_links( array(
                        'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                        'format' => '?paged=%#%',
                        'current' => max( 1, get_query_var('paged') ),
                        'total' => $the_query->max_num_pages
                    ) );
                   ?>
                <?php } ?>

                <?php else: ?>
                  <article>
                    <h2>Sorry...</h2>
                    <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
                  </article>
                </div>
            <?php endif; ?>

                <div class="wpviddy-ads1">
                    <?php echo getOption('ads-banner-home'); ?>
                </div>
                
            </div>

        </div>
        <!-- /.container -->

    </div>
    <!-- /.content-section-a -->
 
    <hr> 
	<!--<a  name="contact"></a>-->
    <div class="banner">
        <div class="container">

            <div class="row">
                <div class="col-lg-6">
                    <h2>Connect to social media:</h2>
                </div>
                <div class="col-lg-6">
                    <div id="share"></div>
                </div>
            </div>
        </div> 
    </div>


    <!-- Footer -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                     
                    <?php if ( has_nav_menu( 'primary' ) ) : ?> 
                            <?php
                                // Primary navigation menu.
                                wp_nav_menu( array(
                                    'menu_class'     => 'list-inline',
                                    'theme_location' => 'primary',
                                ) );
                            ?> 
                    <?php endif; ?>  
                    <p class="copyright text-muted small"> 
                    <?php echo getOption('copyright'); ?>
                    </p>
                </div>
            </div>
        </div>
    </footer>

    <?php require plugin_dir_path( __FILE__ ) . 'footer.php'; ?> 
    <script type="text/javascript">
        var callback = $.cookie('callback');
        var callback_open = $.cookie('callback_open');
        console.log(callback +' == '+ callback_open); 

         <?php 
          $key_single = !empty($_GET['key']) ? $_GET['key'] : '';
         if( $key_single == 123 ) { 
                ?>
                $.cookie('callback_open', callback, { expires: 1, path: '/' } );
                window.location = callback;
                <?php
            } ?>
    </script>

   
</body>
</html>
