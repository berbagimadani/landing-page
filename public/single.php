<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>
        <?php
        $options = get_option('wpviddycpa-option');  
        $slug_custome_post =  !empty($options['slug-custome-post-type']) ? $options['slug-custome-post-type'] : '';
        $slug_taxonomy_post =  !empty($options['slug-taxonomy-post-type']) ? $options['slug-taxonomy-post-type'] : ''; 
            $terms = get_the_terms( get_the_ID(), $slug_taxonomy_post );
            if ( !empty( $terms ) ){ $term = array_shift( $terms ); echo $term->name;  } 
        ?> - <?php echo get_the_title(); ?>
    </title>
    <?php require plugin_dir_path( __FILE__ ) . 'head-style.php'; ?>

</head>

<body>

    <?php 
        $video_id   = get_post_meta(get_the_ID(),'wpviddycpa-meta-url_video',TRUE);
        $player     = get_post_meta(get_the_ID(),'wpviddycpa-meta-player',TRUE); 
        $url_image  = get_post_meta(get_the_ID(),'wpviddycpa-meta-url_image',TRUE);
        $type_cta   = get_post_meta(get_the_ID(),'wpviddycpa-meta-type_cta',TRUE);
        $title      = get_the_title();

        $url_cta   = get_post_meta(get_the_ID(),'wpviddycpa-meta-url_cta',TRUE);
        $locker    = get_post_meta(get_the_ID(),'wpviddycpa-meta-locker',TRUE);
        $url_referral    = get_post_meta(get_the_ID(),'wpviddycpa-meta-url_referral',TRUE);
        $duration_show_offer    = get_post_meta(get_the_ID(),'wpviddycpa-meta-duration_show_offer',TRUE);

        $show_title_offer =  !empty($options['show-title-offer']) ? $options['show-title-offer'] : '';

        $download_type_cta =   get_post_meta(get_the_ID(),'wpviddycpa-meta-download:type_cta',TRUE);
        $download_url_cta =   get_post_meta(get_the_ID(),'wpviddycpa-meta-download:url_cta',TRUE);
        $download_locker =  get_post_meta(get_the_ID(),'wpviddycpa-meta-download:locker',TRUE);
        $download_url_referral    = get_post_meta(get_the_ID(),'wpviddycpa-meta-download:url_referral',TRUE);

        $get_permalink = get_permalink(get_the_ID());
        $view_watch    = get_post_meta(get_the_ID(),'wpviddycpa-meta-view_watch',TRUE);
       
    ?> 
    
    <!-- Navigation -->
    <?php require plugin_dir_path( __FILE__ ) . 'navigation.php'; ?>

    <div  style="padding:10px; background:#fff">
    <div class="container">
    <div class="row"> 
        <div class="col-md-12">
            <span style="font-size:26px"><?php echo get_the_title(); ?></span> 
        </div>
    </div>
    </div>
    </div>

    <!-- Header -->
    <a name="about"></a>
    <div class="intro-header">
        <div class="container">

            <div class="row main-background">
                <div class="col-lg-12 canvas-viddy-desc">
                    <div class="intro-message">

                        <!-- bg image -->
                        <div id="overlay"></div> 
                        <div id="show_data_video"></div>

                        <div class="overlay-second">
                            <div class="videobox">
                                <span id="play-viddy-123-second"></span>
                            </div>
                        </div>
                        <!-- HIDDEN -->
                        <span class="show-offer"></span>
                        <span class="frame_offer_cpa"></span>
                        
                        <!-- openload 
                        <iframe src="https://openload.co/embed/XceYfx2LUfE" width="100%" height="500" frameborder="0" id="video"></iframe>
                        -->
                        <!--<iframe src="https://thumb.openload.co/splash/XceYfx2LUfE/XqGyy_a_OdE.jpg" width="100%" height="500" frameborder="0" id="video_background"></iframe>
                        -->
                        <!-- Youtube And mp4
                        <div class="plyr" id="video"><div data-video-id="NZSxu-UKLV8" data-type="youtube"></div></div> -->

                        <!--
                        <video id="video" poster="https://thumb.openload.co/splash/XceYfx2LUfE/XqGyy_a_OdE.jpg" controls>
                          <source src="http://insideoutfullmovie.net/watch-inside-out-full-movie-online.mp4" type="video/mp4"> 
                           Captions are optional 
                          <track kind="captions" label="English captions" src="/path/to/captions.vtt" srclang="en" default> 
                        </video> -->
                    </div>
                </div>

                <div class="col-md-3" style="text-align:left; padding-bottom:10px;">
                    <a href="<?php echo $get_permalink; ?>"> 
                        <img src="<?php echo get_post_meta(get_the_ID(),'wpviddycpa-meta-image',TRUE); ?>" class="img-responsive">
                    </a>
                </div>
                <div class="col-md-7">
                    <div class="main-desc" style="text-align:left">
                        <h3 class="titleviddy title-generate"><?php echo get_post_meta(get_the_ID(),'wpviddycpa-meta-title',TRUE); ?></h3> 
                        <p class="desc-viddy" id="description"><?php echo get_post_meta(get_the_ID(),'wpviddycpa-meta-description',TRUE); ?></p>

                        <div class="mvic-info">
                            <div class="mvici-left">
                                
                                <?php if(get_post_meta(get_the_ID(),'wpviddycpa-meta-genres',TRUE)) : ?>
                                <p><strong>Genre: </strong><a id="genres"><?php echo get_post_meta(get_the_ID(),'wpviddycpa-meta-genres',TRUE); ?></a></p>
                                <?php endif; ?>

                                <?php if(get_post_meta(get_the_ID(),'wpviddycpa-meta-actors',TRUE)) : ?>
                                <p><strong>Actor: </strong><a id="actors"><?php echo get_post_meta(get_the_ID(),'wpviddycpa-meta-actors',TRUE); ?></a></p> 
                                <?php endif; ?>
                                
                                <?php if(get_post_meta(get_the_ID(),'wpviddycpa-meta-duration',TRUE)) : ?>
                                    <p><strong>Duration:</strong> <span id="duration"><?php echo get_post_meta(get_the_ID(),'wpviddycpa-meta-duration',TRUE); ?></span></p>
                                <?php endif; ?>
                                <?php if(get_post_meta(get_the_ID(),'wpviddycpa-meta-release',TRUE)) : ?>
                                    <p><strong>Release:</strong> <span id="release"><?php echo get_post_meta(get_the_ID(),'wpviddycpa-meta-release',TRUE); ?></span></p>
                                <?php endif; ?>
                                <?php if(get_post_meta(get_the_ID(),'wpviddycpa-meta-rating',TRUE)) : ?>
                                    <p><strong>Rating IMDB:</strong> <span id="rating"><?php echo get_post_meta(get_the_ID(),'wpviddycpa-meta-rating',TRUE); ?></span></p>
                                <?php endif; ?>

                            </div> 
                               
                            <div class="clearfix"></div>
                            <div id="share"></div>
                            <span id="view" style="display:none"></span> 

                        </div>
                    </div>    
                </div>
                <div class="col-md-2"> 
                    <div class="pull-right">
                     <span class="btn-download-viddy"></span> 
                    </div>
                </div>

            </div>

        </div>
        <!-- /.container -->

    </div>
    <!-- /.intro-header -->

    <!-- Page Content -->

	<div class="container">
        <div class="row no-getter"> 
            <div class="col-md-12 fb-comment"> 
                <div class="fb-comments" data-href="<?php echo $get_permalink; ?>" data-width="100%" data-numposts="5"></div>
            </div>
        </div>
    </div>
    <div class="content-section-a">

        <div class="container">
            <div class="row no-getter"> 
                <div class="col-md-12">
                    <h3>You May Also Like</h3>  
                </div>
                 <?php 

                   //$currentCategory = single_cat_title("", false);
                    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                    $args = array(
                        'post_type' => SLUG_CUSTOME_POST,
                        //'gallery_category' => 'video-gallery',
                        'paged'=> $paged,
                        'order' => 'DESC',
                        'posts_per_page' => '12'
                    );  
                    $the_query = new WP_Query( $args ); 
            if($the_query->have_posts()) :     

                    while ($the_query->have_posts()):
                        $the_query->the_post();   
                        $featured_image_array = wp_get_attachment_image_src( get_post_thumbnail_id(), 'single-post-thumbnail' );
                        $featured_image = $featured_image_array[0];
                   ?>  
                <div class="col-md-2 col-xs-4 box-thumb-images"> 
                    <div class="thumbnail no-border">
                        <a href="<?php echo get_permalink(); ?>">
                        <div class="caption">
                            <h5><?php echo get_post_meta(get_the_ID(),'wpviddycpa-meta-title',TRUE) ?> </h5>  
                            <p style="font-size:11px">
                             <?php 
                                $text = get_post_meta(get_the_ID(),'wpviddycpa-meta-description',TRUE);
                                echo wp_trim_words( $text, $num_words = 18, $more = null );
                              ?> 
                            </p>
                            <p>
                            <span class="label label-danger" rel="tooltip" title="Release year"><?php echo get_post_meta(get_the_ID(),'wpviddycpa-meta-release',TRUE) ?> </span>
                            </p>
                            <p>
                            <span class="label label-viddy-orange" rel="tooltip" title="IMDB Rate">
                            IMDb:
                            <?php echo get_post_meta(get_the_ID(),'wpviddycpa-meta-rating',TRUE) ?>
                            </span>
                            </p>
                        </div>
                        <img class="img-responsive" src="<?php echo get_post_meta(get_the_ID(),'wpviddycpa-meta-image',TRUE) ?>">
                        </a>
                    </div>
                    <a href="<?php echo get_permalink(); ?>" class="text-center">
                        <h5><?php echo get_post_meta(get_the_ID(),'wpviddycpa-meta-title',TRUE) ?> </h5> 
                    </a>
                </div>
                <?php endwhile; ?>
                
                <div class="col-md-12">
                <?php if ($the_query->max_num_pages > 1) { // check if the max number of pages is greater than 1  ?>
                   <?php
                   /* $big = 999999999; // need an unlikely integer
                    echo paginate_links( array(
                        'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                        'format' => '?paged=%#%',
                        'current' => max( 1, get_query_var('paged') ),
                        'total' => $the_query->max_num_pages
                    ) );*/
                   ?>
                <?php } ?>

                <?php else: ?>
                  <article>
                    <h2>Sorry...</h2>
                    <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
                  </article>
                </div>
            <?php endif; ?>
            
            <div class="wpviddy-ads1">
                <?php echo getOption('ads-banner-single'); ?>
            </div>

            </div>
        </div>
        <!-- /.container -->

    </div>
    <!-- /.content-section-a -->
 
    <hr> 
	<!--<a  name="contact"></a>
    <div class="banner">

        <div class="container">

            <div class="row">
                <div class="col-lg-6">
                    <h2>Connect to Start Bootstrap:</h2>
                </div>
                <div class="col-lg-6">
                    <ul class="list-inline banner-social-buttons">
                        <li>
                            <a href="https://twitter.com/SBootstrap" class="btn btn-default btn-lg"><i class="fa fa-twitter fa-fw"></i> <span class="network-name">Twitter</span></a>
                        </li>
                        <li>
                            <a href="https://github.com/IronSummitMedia/startbootstrap" class="btn btn-default btn-lg"><i class="fa fa-github fa-fw"></i> <span class="network-name">Github</span></a>
                        </li>
                        <li>
                            <a href="#" class="btn btn-default btn-lg"><i class="fa fa-linkedin fa-fw"></i> <span class="network-name">Linkedin</span></a>
                        </li>
                    </ul>
                </div>
            </div>

        </div> 

    </div>-->


    <!-- Footer -->
    <footer>
        <div class="container">
        
            <div class="row">
                <div class="col-lg-12">
                     <?php if ( has_nav_menu( 'primary' ) ) : ?> 
                            <?php
                                // Primary navigation menu.
                                wp_nav_menu( array(
                                    'menu_class'     => 'list-inline',
                                    'theme_location' => 'primary',
                                ) );
                            ?> 
                    <?php endif; ?>  
                    <p class="copyright text-muted small">
                         <?php echo getOption('copyright'); ?>
                    </p>
                </div>
            </div>
        </div>
    </footer>

<?php require plugin_dir_path( __FILE__ ) . 'footer.php'; ?>


</body>
</html>
